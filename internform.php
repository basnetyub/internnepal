<?php include 'part/config_global.php' ?>
<!DOCTYPE html>
<html>
<head>
    <title>Intern Form -- Intern Nepal</title>
    <?php include 'part/global_meta.php' ?>
    <meta name="description" content="Fill this form to provide information about your company and the type of intern that you want.">
   	<?php include 'part/cssdependencies.php' ?>
</head>
<?php include 'part/wrapper_top.php' ?>
       <?php include 'part/internform_core.php' ?>         
<?php include 'part/wrapper_bottom.php' ?>
<?php include 'part/jsdependencies.php' ?>
<script src="javascript/internform.js"></script>
</html>
