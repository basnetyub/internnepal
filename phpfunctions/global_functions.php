<?php
        function handleOther($othertext){
            $len = strlen($othertext);
            if($len>5){
                $pos = strpos($othertext,")")-7;
                $othertext = substr($othertext,7,$pos);
            }
            return $othertext;
        }
        function handleFullyOther($othertext){
            if(preg_match("/(other)/i", $othertext) === 1){
                $othertext = handleOther($othertext);
            }
            return $othertext;
        }
        function days_until($date){
            $rightnow = time();
            $unixtime = strtotime($date);
            $diffis = ($unixtime - $rightnow);
            if($diffis>31557600 || $unixtime==NULL){
                if ($diffis>630720000 || $unixtime==NULL){
                    return "20 year(s)"."+";
                }
                else{
                    $diffisyrs = floor($diffis/31557600);
                    if($diffisyrs == 1){
                    return (floor($diffis/60/60/24)+1)." [".$diffisyrs." year"."+]";
                    }
                    else{
                    return (floor($diffis/60/60/24)+1)." [".$diffisyrs." year(s)"."+]";
                    }
                }
            }
            else{
                return (isset($date)) ? floor($diffis/60/60/24)+1 : FALSE;
            }
        }
        function handleNull($othertext){
            if($othertext==NULL){
                return "N/A";
            }
            return $othertext;
        }
        function showDeadlineHelpText($thedeadline){
            $days_untilthedeadline = days_until($thedeadline);
            if ($days_untilthedeadline<=7){
                if($days_untilthedeadline>1){
                    $days_untilthedeadline = $days_untilthedeadline." days until deadline";
                }
                else if($days_untilthedeadline==1){
                    $days_untilthedeadline = $days_untilthedeadline." day until deadline";
                }
                else if($days_untilthedeadline==0){
                    $days_untilthedeadline = "Today is the deadline!";
                }
                
                echo '<span id="isdeadlineapproaching" data-toggle="tooltip" data-placement="right" title="'.$days_untilthedeadline.'"></span>';
            }
        }
    ?>