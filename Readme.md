#INSTRUCTIONS
---
Write, under titles, Descending by Date and Time.

Writing Format:

* DATE TIME **STATUS**`@Name`
    * ISSUE  and `block of code if required`


After Issue is fixed, add the status **FIXED**`@Name`. Otherwise keep **OPEN**`@Name`. If you want to reopen a closed status, **OPEN**`@Name2@Name`

Example:

* Open Status looks like this **OPEN**@siddhantrimal
* Fixed Status looks like this **OPEN**@siddhantrimal **FIXED**@basnetyub . Similarly to close a depreciated issue, **OPEN**@siddhantrimal **CLOSED**@basnetyub
* Re-Opened Status looks like this **OPEN**@pratikmetal3 @siddhantrimal **CLOSED**@basnetyub

---
#DEPENDENCIES
---
*Important information about the things required to operate the website*

* 5/16/2017 **UPDATE**@siddhantrimal Now InternNepal will work only on testtt database version 7. On any older version of that database, it shall crash or behave unexpectedly. This is because APPROVED/VERIFIED field is added to company table and APPROVED is added to internship table too. Also request_std table is created for Request Form.

* 5/14/2017 **UPDATE**@siddhantrimal Now InternNepal will manage its two Database connection credentials through part/config_database.php . From Now on, please do not add database config hardcoded into the PHP code.

* 4/29/2017 **UPDATE**@siddhantrimal To Manage everything better, and to provide linear updates to all pages, PHP segments has been broken down. New general format is: (Example for index.html)
```HTML
<!DOCTYPE html>
<html>
<head>
    <title>Intern Nepal</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
   	<?php include 'part/cssdependencies.php' ?>
</head>
<?php include 'part/wrapper_top.php' ?>
                <?php include 'part/home.php' ?>
<?php include 'part/wrapper_bottom.php' ?>
<?php include 'part/jsdependencies.php' ?>
</html>

```
   * Instead of `<?php include 'part/home.php' ?>`, other developers can use (say)`<?php include 'part/services_data.php' ?>` in the services.php page

---
#To-DO LIST
---
*These are things that need immediate attention and must be absolutely completed before launch*

* Database
    * Structure
        * Our Db is naturally fucked up. We need to come around and discuss a lot of things about how to proceed. No ghachak-ghuchuk work!!!
    * Security **OPEN**@siddhantrimal
        * While it is Okay for operation for now, we should definitely convert all sqli statements to prepared statements whenever possible. This prevents SQL injection attack, which our Db is not protected from, right now.
* Forms
    * Intern-Form **OPEN**@siddhantrimal
        * Almost Completed. Works but has minor issues. Also, Validation is remaining. Security Loophole due to no validation allows empty php to populate company and internship table data in SQL Db
        * Intern-Form also has no provision of storing IMAGES for Company Logo. We need to fix that ASAP. Can't be showing Images hard coded in html in the market, like last time lol
    * Request-Form **OPEN**@siddhantrimal
        * Broken. Needs Fixing. Easy to Fix Now. Many jQuery issues have already been resolved while cleaning up for Intern-Form issue.
    * Contact-Modal **OPEN**@siddhantrimal
        * Broken. Needs Fixing. I'm pretty sure whats wrong with this one. I'll come around to fix it when I have time. Or any of you can fix and close this issue.

---
#ISSUES
---

* 5/16/2017 1:34 **OPEN**@siddhantrimal
    * When I disabled SQL Server, the InternForm produced this error
	```
    Warning: mysqli::__construct(): (HY000/2002): No connection could be made because the target machine actively refused it. 
    in C:\XAMPP\htdocs\internnepal\part\internform_backend.php on line 50
    Connection failed: No connection could be made because the target machine actively refused it.
    ```
    * I realize that you are thinking "thulokura haina" tara we seriously need to do something other than just suppress these warning before we go LIVE. Suppress garna ta aucha. Simple cha. error_reporting(0) garney ho. Tara I've seen error reporting options in cpanel. Tyo Level ma nagarney vayepani, we need to send errors somewhere. All errors must be formatted in such a way that we can know which file is sending it too. Pachi problem aucha aucha sure chu ma.

* 5/15/2017 11:58 **OPEN**@siddhantrimal
    * Internform_backend.php will INSERT into database, the new data for comapany. But for some instances, the Internship table is being rejected. I suspect, this is because of datatype problem. I encountered this error when I put long descriptions, like a real company would, instead of small test data.

* 5/15/2017 10:31 **OPEN**@siddhantrimal
    * The empty entries show 17301days ago in the expired count. If expired can be removed, that is not an issue. If it cannot be filtered on SQL end, then someone should think up a solution. I think the date is counting UNIX time. Solutions should be similar. 

* 5/15/2017 10:30 **OPEN**@siddhantrimal
    * There is currently no protection against running part/internform_backend.php without $_POST any data. This means, anyone can just keep refreshing the webpage www.internnepal.com/part/internform_backend.php and that will execute with empty parameters and fill the table with NULL values. This is a security loophole. Need to look at this later. One possible solution, that I can think of, is keeping internform_backend outside public_html. Say maybe, this location ../../data/internform_backend.php . I wonder if it executes with AJAX at that position, because Browser cannot reach there.