<?php include 'part/config_global.php' ?>
<!DOCTYPE html>
<html>
<head>
    <title>Description of Training Institute -- Intern Nepal</title>
    <?php include 'part/global_meta.php' ?>
    <meta name="description" content="Get contact and general information about training institutes, affiliated with us, and available courses.">
    <?php include 'part/cssdependencies.php' ?>
</head>
<?php include 'part/wrapper_top.php' ?>
       <?php include 'part/traininginstitutedescriptiontable.php' ?>         
<?php include 'part/wrapper_bottom.php' ?>
<?php include 'part/jsdependencies.php' ?>
</html>