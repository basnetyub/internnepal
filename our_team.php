<?php include 'part/config_global.php' ?>
<!DOCTYPE html>
<html>
<head>
    <title>Our Team -- Intern Nepal</title>
    <?php include 'part/global_meta.php' ?>
    <meta name="description" content="Meet the awesome people behind Intern Nepal.">
    <?php include 'part/cssdependencies.php' ?>
</head>
<?php include 'part/wrapper_top.php' ?>
       <?php include 'part/team.php' ?>         
<?php include 'part/wrapper_bottom.php' ?>
<?php include 'part/jsdependencies.php' ?>
</html>