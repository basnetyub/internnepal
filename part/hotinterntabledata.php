<?php include 'part/config_database.php' ?>
<?php
$hotinternships = array();
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "select c.c_name, i.intern_id, i.intern_title, i.internship_category, c.c_verified, i.deadline
from company as c inner join internship as i 
on c.c_id=i.c_id 
AND c.c_approved=1
AND i.deadline >= '".date("Y-m-d")."'
order by i.intern_id desc limit 5;";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        $c_name = $row["c_name"];
        $intern_title = $row["intern_title"];
        $internship_category = $row["internship_category"];
        $internship_category = handleFullyOther($internship_category);
        array_push($hotinternships, $id=$row["intern_id"]);
?>
<?php echo '<a href="interndescription.php?id='.$id.'">';?>
            <div class="pure-g hotinternshipsideshow">
                <div class="pure-u-8-24">
                    <img src="images/logo.svg" class="pure-img">
                </div>
                <div class="pure-u-16-24 hotinternshipsideshowinfo">
                    <div class="pure-g">
                        <div class="pure-u-1">
                            <?php echo $c_name; 
                                if(strcmp($row['c_verified'],"1")==0){
                                  echo '<span id="iscompanyverified" data-toggle="tooltip" data-placement="right" title="Verified by InternNepal"></span>';  
                                }
                                showDeadlineHelpText($row["deadline"]);
                            ?><!-- Insert Company Name for Database here-->
                        </div>
                        <div class="pure-u-1">
                            <?php echo $internship_category; ?>
                        </div>
                        <div class="pure-u-1 worktag">
                            <?php echo $intern_title; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- segment-2 END -->
<?php echo '</a>';?>
<?php 
    }
} else {
    echo "0 results";
}
$conn->close();
?>