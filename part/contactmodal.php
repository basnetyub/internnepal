<div class="modal hide fade" tabindex="-1" id="contactus" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Drop a quick message</h4>
                <div class="buttonColors"><button type="button" class="close" data-dismiss="modal">&times;</button></div>
            </div>
            <div class="modal-body" id="contactmodalformbody">
                <p>Got a burning question that's bugging you? Don't wait! Drop us a quick query with this form.</p>
                <form class="pure-form pure-form-aligned" id="contactmodalform">
                    <fieldset>
                        <legend>Fill this form</legend>
                        <div class="form-group row">
                            <span class="col-md-1 offset-md-2 text-xs-center"><i class="fa fa-user bigicon"></i></span>
                            <div class="col-md-8">
                                <input id="fname" name="name" type="text" placeholder="Full Name" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <span class="col-md-1 offset-md-2 text-xs-center"><i class="fa fa-envelope-o bigicon"></i></span>
                            <div class="col-md-8">
                                <input id="email" name="email" type="email" placeholder="Your Email" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <span class="col-md-1 offset-md-2 text-xs-center"><i class="fa fa-pencil-square bigicon"></i></span>
                            <div class="col-md-8">
                                <input id="subject" name="subject" type="text" placeholder="Subject" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <span class="col-md-1 offset-md-2 text-xs-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
                            <div class="col-md-8">
                                <textarea class="form-control" id="message" name="message" placeholder="Please type your message here" rows="7" required></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 d-flex justify-content-center">
                                <button type="submit" name="submit" class="pure-button pure-button-primary button-xlarge"><i class="fa fa-paper-plane" aria-hidden="true"></i>&#160;Submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <p><i class="fa fa-info-circle" aria-hidden="true"></i>&#160;Businesses and Institutions should formally contact us at <a href="mailto:contactus@internnepal.com">contactus@internnepal.com</a></p>
            </div>
        </div>
    </div>
</div>
