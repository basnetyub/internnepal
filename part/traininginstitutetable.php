<?php include 'part/config_database.php' ?>
<?php
$pagination = 0; //get value of pagination and set here
$count = $pagination*10;

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$coursespecific=$_GET['coursespecific'];

if((strcasecmp ( $coursespecific ,"it")==0 ||
strcasecmp ( $coursespecific ,"social")==0 ||
strcasecmp ( $coursespecific ,"hotel")==0 ||
strcasecmp ( $coursespecific ,"admin")==0 ||
strcasecmp ( $coursespecific ,"other")==0)){
$sql = "select ti.ti_name,ti.ti_ctype, ac.ac_title, ti.ti_id, ti.ti_website
from training_institute as ti
inner join available_courses as ac on ti.ti_id=ac.ti_id where ti.ti_ctype like '%".$coursespecific."%';";
}
else{
$sql = "select ti.ti_name,ti.ti_ctype, ac.ac_title, ti.ti_id, ti.ti_website 
from training_institute as ti
inner join available_courses as ac on ti.ti_id=ac.ti_id;";
}
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) { 
        ?>
                    <div class="pure-g">
                    <div class="pure-u-1 hideScrambledEggs" id="headertargetpadding">&nbsp;</div>
                        <div class="pure-u-1 pure-u-sm-1-4 grid-item">
                            <div class="pure-g">
                                <div class="pure-u-1 grid-label labelBackdrop showBoxHead">
                                <!-- Db: Show Available SN number-->Training Institute #<?php echo $count=$count+1; ?></div>
                            </div>
                            <div class="pure-g">
                                <div class="pure-u-1-3 sideDescriptor">Company</div>
                                <div class="pure-u-2-3 mainItem">
                                    <!-- <p> -->
                                    <!-- <a href=" //www.blackhawk.edu.np/" target="_blank"> -->
                                    <?php echo '<a href="about_traininginstitute.php?id='.$row["ti_id"].'">'.$row["ti_name"].'</a>' ?><!-- </a> -->
                                    <!-- </p> -->
                                </div>
                            </div>
                        </div>
                        <!-- 3 -->
                         <div class="pure-u-1 pure-u-sm-1-6 grid-item">
                            <div class="pure-g">
                                <div class="pure-u-1-3 sideDescriptor">Category</div>
                                <div class="pure-u-2-3 mainItem">
                                    <!-- <p> -->
                                    <?php 
                                    $ti_cat = $row["ti_ctype"];
                                    if(preg_match("/(it|computer)/i", $ti_cat) === 1){
                                        $groupto="it";
                                    }
                                    else if(preg_match("/(social|ngo)/i", $ti_cat) === 1){
                                        $groupto="social";
                                    }
                                    else if(preg_match("/(hotel)/i", $ti_cat) === 1){
                                        $groupto="hotel";
                                    }
                                    else if(preg_match("/(admin|administration)/i", $ti_cat) === 1){
                                        $groupto="admin";
                                    }
                                    if(preg_match("/(other)/i", $ti_cat) === 1){
                                        $groupto="other";
                                    }
                                    echo '<a href="?coursespecific='.$groupto.'"  style="color:brown;" rel="follow">'.handleFullyOther($ti_cat).'</a>';
                                    $groupto =$row["ti_ctype"];
                                    ?>
                                    <!-- </p> -->
                                </div>
                            </div>
                        </div>
                        
                        <!-- 2 -->
                        <div class="pure-u-1 pure-u-sm-14-24 grid-item">
                            <div class="pure-g">
                                <div class="pure-u-1-3 sideDescriptor">Courses</div>
                                <div class="pure-u-2-3 mainItem">
                                    <!-- <p> -->
                                    <?php  echo $row["ac_title"] ?>
                                    <!-- </p> -->
                                </div>
                            </div>
                            <div class="pure-g">
                                <div class="pure-u-1 grid-label labelBackdropBottom showBoxHead"></div>
                            </div>
                        </div>
                        <!-- 4 -->
                       
                        <!-- 2 -->
                        
                    </div>
                    <hr class="itemseperator">


    <?php 
}

} else {
    echo "0 results";
}
//FILTRATION SYSTEM---------------------
if(strcasecmp ( $coursespecific ,"it")==0){$filtergroup="IT/Computer Science";}
else if(strcasecmp ( $coursespecific ,"social")==0){$filtergroup="Social Work";}
else if(strcasecmp ( $coursespecific ,"hotel")==0){$filtergroup="Hotel";}
else if(strcasecmp ( $coursespecific ,"admin")==0){$filtergroup="Administration";}
else if(strcasecmp ( $coursespecific ,"other")==0){$filtergroup="Other";}
if((strcasecmp ( $coursespecific ,"it")==0 ||
strcasecmp ( $coursespecific ,"social")==0 ||
strcasecmp ( $coursespecific ,"hotel")==0 ||
strcasecmp ( $coursespecific ,"admin")==0 ||
strcasecmp ( $coursespecific ,"other")==0)){
echo ' <div class="pure-u-1" style="font-size:.9em; padding:5px;">Results filtered by : '.$filtergroup.'<a href="training_institute.php"><span class="fa-stack fa-lg" data-toggle="tooltip" data-placement="bottom" title="remove filter">
  <i class="fa fa-filter fa-stack-1x" style="color: rgba(0,0,0,1);"></i>
  <i class="fa fa-ban fa-stack-2x" style="color: rgba(217,83,79,0.5);"></i>
</span></a></div>';
}
//----------------------
$conn->close();
?>



