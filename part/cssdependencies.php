<link rel="stylesheet" href=" //maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href=" //fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
<link href=" //fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
<link href=" //fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
<!--Bootstrap 4 CDN-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<!-- SWAL -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/sweetalert2/6.5.6/sweetalert2.min.css">
<!-- purecss -->
<link rel="stylesheet" href="https://unpkg.com/purecss@0.6.2/build/pure-min.css" integrity="sha384-UQiGfs9ICog+LwheBSRCt1o5cbyKIHbwjWscjemyBMT9YCUMZffs6UqUTd0hObXD" crossorigin="anonymous">
<link rel="stylesheet" href="https://unpkg.com/purecss@0.6.2/build/grids-responsive-min.css">
<!-- custom-->
<link rel="stylesheet" type="text/css" href="css/custom.css">
<link rel="stylesheet" type="text/css" href="css/painter.css">
<link rel="icon" type="image/png" href="images/icon.png">

