<?php include 'part/config_database.php' ?>
<?php
$pagination = 0; //get value of pagination and set here
$count = $pagination*10;
$randpos = rand ($pagination, 5);
/*Define how many hot internship marks you want to show.
This field is automatically bound by count of $hotinternships array*/
$hotinternshipsmarklimit= 5; 
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$coursespecific=$_GET['coursespecific'];

if((strcasecmp ( $coursespecific ,"it")==0 ||
strcasecmp ( $coursespecific ,"social")==0 ||
strcasecmp ( $coursespecific ,"hotel")==0 ||
strcasecmp ( $coursespecific ,"admin")==0 ||
strcasecmp ( $coursespecific ,"other")==0)){
$sql = "select c.c_name,c.website, i.intern_id, i.internis, i.intern_title, i.internship_category,i.deadline, c.c_verified 
from company as c
inner join internship as i on c.c_id=i.c_id
where i.internship_category like '%".$coursespecific."%'
AND c.c_approved=1
AND i.i_approved=1
AND i.deadline >= '".date("Y-m-d")."'
order by i.deadline asc;";
}
else{
$sql = "select c.c_name,c.website, i.intern_id, i.internis, i.intern_title, i.internship_category,i.deadline, c.c_verified 
from company as c
inner join internship as i on c.c_id=i.c_id 
AND c.c_approved=1
AND i.i_approved=1
AND i.deadline >= '".date("Y-m-d")."'
order by i.deadline asc;";
}
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) { 
        ?>
                    <?php
                    if((strcasecmp ( $coursespecific ,"it")==0)){$sendto="it";}
                    else if((strcasecmp ( $coursespecific ,"social")==0)){$sendto="social";}
                    else if((strcasecmp ( $coursespecific ,"hotel")==0)){$sendto="hotel";}
                    else if((strcasecmp ( $coursespecific ,"admin")==0)){$sendto="admin";}
                    else if((strcasecmp ( $coursespecific ,"other")==0)){$sendto="other";}
                    else{$sendto="none";}
                    if ($count==$randpos && $sendto!="none"){
                    echo '<div class="pure-g tilbd">
                                <div class="pure-u-1 grid-label labelBackdrop showBoxHead" style="">Training Institute&nbsp;</div>
                            </div>
                    <div class="pure-g">
                        <div class="pure-u-1 ti_grid grid-item"><div><a href="training_institute.php?coursespecific='.$sendto.'">
                        Get the competitive edge. Stand Out from the rest, in your desired field, with professional training</a></div>
                        </div>
                    </div>
                    <div class="pure-g tilbd">
                                <div class="pure-u-1 grid-label labelBackdropBottom showBoxHead" style=""></div>
                            </div><hr class="itemseperator">';
                    } ?>
                    
                    <div class="pure-g">
                    <div class="pure-u-1 hideScrambledEggs" id="headertargetpadding">&nbsp;</div>
                        <div class="pure-u-1 pure-u-sm-1-4 grid-item">
                            <div class="pure-g">
                                <div class="pure-u-1 grid-label labelBackdrop showBoxHead">
                                <!-- Db: Show Available SN number-->Available #<?php echo $count=$count+1; ?></div>
                            </div>
                            <div class="pure-g">
                                <div class="pure-u-1-3 sideDescriptor">Company</div>
                                <div class="pure-u-2-3 mainItem">
                                    <!-- <p> -->
                                    <!-- <a href=" //www.blackhawk.edu.np/" target="_blank"> -->
                                    <?php echo '<a href="interndescription.php?id='.$row["intern_id"].'">'.$row["c_name"].'</a>';
                                            if(strcmp($row['c_verified'],"1")==0){
                                              echo '<span id="iscompanyverified" data-toggle="tooltip" data-placement="right" title="Verified by InternNepal" class="help-verification" onClick="showHelp(this.className)"></span>';  
                                            }
                                            $count = 0;
                                            while($count<count($hotinternships) && $count<$hotinternshipsmarklimit){
                                                if($hotinternships[$count]==$row["intern_id"]){
                                                    echo '<span id="internshipishot" data-toggle="tooltip" data-placement="right" title="Hot! Recently Posted" class="help-hotinternship" onClick="showHelp(this.className)"></span>';  
                                                }
                                                $count=$count+1;
                                            }
                                     ?><!-- </a> -->
                                    <!-- </p> -->
                                </div>
                            </div>
                        </div>
                        <!-- 3 -->
                        <div class="pure-u-1 pure-u-sm-1-6 grid-item">
                            <div class="pure-g">
                                <div class="pure-u-1-3 sideDescriptor">Internship Type</div>
                                <div class="pure-u-2-3 mainItem">
                                    <!-- <p> -->
                                    <?php
                                    	$internis = $row["internis"];
                                    	if(preg_match("/(other)/i", $internis) === 1){
                                    		$internis = handleOther($internis);
                                    	}
                                    	echo $internis;
                                        if(strcmp($internis,"Mentorship")==0){
                                              echo '<sup data-toggle="tooltip" data-placement="right" title="Click to show help" class="help-mentorship" onClick="showHelp(this.className)"></sup>';  
                                            }
                                     ?>
                                    <!-- </p> -->
                                </div>
                            </div>
                        </div>
                        <!-- 2 -->
                        <div class="pure-u-1 pure-u-sm-7-24 grid-item">
                            <div class="pure-g">
                                <div class="pure-u-1-3 sideDescriptor">Intern Title</div>
                                <div class="pure-u-2-3 mainItem">
                                    <!-- <p> -->
                                    <?php  echo $row["intern_title"] ?>
                                    <!-- </p> -->
                                </div>
                            </div>
                        </div>
                        <!-- 4 -->
                        <div class="pure-u-1 pure-u-sm-1-6 grid-item">
                            <div class="pure-g">
                                <div class="pure-u-1-3 sideDescriptor">Category</div>
                                <div class="pure-u-2-3 mainItem">
                                    <!-- <p> -->
                                    <?php 
                                    $internship_category = $row['internship_category'];
                                    if(preg_match("/(it|computer)/i", $internship_category) === 1){
                                        $groupto="it";
                                    }
                                    else if(preg_match("/(social|ngo)/i", $internship_category) === 1){
                                        $groupto="social";
                                    }
                                    else if(preg_match("/(hotel)/i", $internship_category) === 1){
                                        $groupto="hotel";
                                    }
                                    else if(preg_match("/(admin|administration)/i", $internship_category) === 1){
                                        $groupto="admin";
                                    }
                                    else if(preg_match("/(other)/i", $internship_category) === 1){
                                        $groupto="other";
                                    	$internship_category = handleOther($internship_category);
                                    }
                                    echo '<a href="?coursespecific='.$groupto.'"  style="color:brown;" rel="follow">'.$internship_category.'</a>' 
                                    ?>
                                    <!-- </p> -->
                                </div>
                            </div>
                        </div>
                        <!-- 2 -->
                        <div class="pure-u-1 pure-u-sm-3-24 grid-item">
                            <div class="pure-g">
                                <div class="pure-u-1-3 pure-u-sm-1 sideDescriptor" id="deadlinetitle">Deadline</div>
                                <div class="pure-u-2-3 pure-u-sm-1 mainItem">
                                    <!-- <p> -->
                                    <?php 
                                    $thedeadline = $row["deadline"];
                                    echo $thedeadline;
                                    showDeadlineHelpText($thedeadline);
                                    ?>
                                    <!-- </p> -->
                                </div>
                            </div>
                            <div class="pure-g">
                                <div class="pure-u-1 grid-label labelBackdropBottom showBoxHead"></div>
                            </div>
                        </div>
                        <!-- 1 -->
                    </div>
                    <hr class="itemseperator">

    <?php 
}
} else {
    echo "0 results";
}
//FILTRATION SYSTEM---------------------
if(strcasecmp ( $coursespecific ,"it")==0){$filtergroup="IT/Computer Science";}
else if(strcasecmp ( $coursespecific ,"social")==0){$filtergroup="Social Work";}
else if(strcasecmp ( $coursespecific ,"hotel")==0){$filtergroup="Hotel";}
else if(strcasecmp ( $coursespecific ,"admin")==0){$filtergroup="Administration";}
else if(strcasecmp ( $coursespecific ,"other")==0){$filtergroup="Other";}
if((strcasecmp ( $coursespecific ,"it")==0 ||
strcasecmp ( $coursespecific ,"social")==0 ||
strcasecmp ( $coursespecific ,"hotel")==0 ||
strcasecmp ( $coursespecific ,"admin")==0 ||
strcasecmp ( $coursespecific ,"other")==0)){
echo ' <div class="pure-u-1" style="font-size:.9em; padding:5px;">Results filtered by : '.$filtergroup.'<a href="index.php"><span class="fa-stack fa-lg" data-toggle="tooltip" data-placement="bottom" title="remove filter">
  <i class="fa fa-filter fa-stack-1x" style="color: rgba(0,0,0,1);"></i>
  <i class="fa fa-ban fa-stack-2x" style="color: rgba(217,83,79,0.5);"></i>
</span></a></div>';
}
//----------------------
$conn->close();
?>



