<style>
.requiredfield::after{
content: '*';
color:rgba(210,0,0,.8);
}
div.options > label > input {
/*visibility: hidden;*/
height:.1em;
width: .1em;
margin:20px -15px 0 10px;
}
div.options > label {
display: block;
/*margin: 0 0 0 -10px;*/
padding: 5px 0 20px 0;
height: 20px;
/*width: 150px;*/
}
div.options > label > img {
display: inline-block;
padding: 0px;
height:30px;
width:30px;
background: none;
}
div.options > label > input:not(:checked) +img {
background: #ffffff;
}
div.options > label > input:checked +img {
background: url(images/form_tick.png);
background-color: #ffffff;
background-repeat: no-repeat;
background-position:center center;
background-size:30px 30px;
}
</style>
<div class="pure-g" style="padding:20px 10px 30px 10px; border:1px solid rgba(90,41,125,.1); background-color: #eeeeff;">
    <div class="pure-u-1">
        <div id="form-content">
            <div style="padding-bottom: 10px;">
                <div style="background-color: #1d2d44; margin: -20px -10px 0 -10px; padding:5px 10px 1px 10px; color: #eeeeff;">
                    <p class="lead" style=" font-size: 1.2em!important;">Fill the following form detailing your company and the type of interns you seek.</p>
                </div></div>
                <!-- <div id="form-content"> -->
                <form class="pure-form" id="reg-form" autocomplete="on">
                    <fieldset>
                        <legend name="legend1">Company Details:</legend>
                        <div class="form-group">
                            <label for="Company Name" class="requiredfield">Full Name of Company/Office/Organization:</label>
                            <input type="text" class="form-control" id="cname" name="cname" placeholder="Enter Company Name" required="required">
                        </div>
                        <div class="form-group options">
                            <label for="Company Type" class="requiredfield">Type of Company(Please tick):</label>
                            <label title="IT">
                                <input type="radio" name="ctype" id="ctypeit" value="IT/Computer Science" required/>
                                <img /> IT/Computer Science
                            </label>
                            <label title="Social">
                                <input type="radio" name="ctype" id="ctypeorg" value="Organization(NGO/INGO)" />
                                <img /> Organization(NGO/INGO)
                            </label>
                            <label title="Hotel">
                                <input type="radio" name="ctype" id="ctypehotel" value="Hotel/Administration" />
                                <img /> Hotel/Administration
                            </label>
                            <label title="Other">
                                <input type="radio" name="ctype" id="ctypeother" value="Other" id="ctypeother"/>
                                <img /> Other
                                <div style="display: inline-block;">
                                    <input type="text" class="form-control" name="ctypeother" id="ctypeotherwhat" placeholder="Please Specify" required>
                                </div>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="Location" class="requiredfield">Location:</label>
                            <input type="text" class="form-control" id="location" name="location" placeholder="Enter Address" required>
                        </div>
                        <div class="pure-g">
                            <div class="pure-u-1 pure-u-md-8-24">
                                <div class="form-group">
                                    <label for="Contact No" class="requiredfield">Contact No</label>
                                    <input type="tel" class="form-control" id="internform_contact" name="contact" placeholder="Enter Contact Number" required>
                                </div>
                            </div>
                            <div class="pure-u-1 pure-u-md-16-24">
                                <div class="form-group">
                                    <label for="Email">Email:</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter Company's Email">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="Website">Website</label>
                            <input type="text" class="form-control" id="website" name="website" placeholder="What is the Company's Website?">
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Internship Details:</legend>
                        <div class="form-group">
                            <label for="intern_title" class="requiredfield">Intern Title</label>
                            <input type="text" class="form-control" id="intern_title" name="intern_title" placeholder="Enter internship title for the Intern. Separate multiple titles by comma (,)" required>
                        </div>
                        <div class="pure-g">
                            <div class="pure-u-1 pure-u-md-1-2" style="margin-bottom: 2em;vertical-align: top;">
                                <div class="form-group options">
                                    <label for="Internship Category" class="requiredfield">Internship Category(Please tick):</label>
                                    <label title="IT/Computer Science">
                                        <input type="radio" name="internship_category" value="IT/Computer Science" id="internship_category_it" required/>
                                        <img /> IT/Computer Science
                                    </label>
                                    <label title="Social Work">
                                        <input type="radio" name="internship_category" value="Social Work" id="internship_category_social"/>
                                        <img /> Social Work
                                    </label>
                                    <label title="Hotel">
                                        <input type="radio" name="internship_category" value="Hotel" id="internship_category_hotel"/>
                                        <img /> Hotel
                                    </label>
                                    <label title="Administration">
                                        <input type="radio" name="internship_category" value="Administration" id="internship_category_admin"/>
                                        <img /> Admininstration
                                    </label>
                                    <label title="Other">
                                        <input type="radio" name="internship_category" value="Other" id="internship_category_other"/>
                                        <img /> Other
                                        <div style="display: inline-block;">
                                            <input type="text" class="form-control" name="internship_category_other" id="internship_category_other_what" placeholder="Please Specify" required>
                                        </div>
                                    </label>
                                </div>
                            </div>
                            <div class="pure-u-1 pure-u-md-1-2" style="margin-bottom: 2em;vertical-align: top;">
                                <div class="form-group options">
                                    <label for="Remuneration Type" class="requiredfield">Remuneration Type</label>
                                    <label title="Paid">
                                        <input type="radio" name="internis" value="Paid" id="internis_paid" required/>
                                        <img /> Paid
                                    </label>
                                    <label title="Unpaid">
                                        <input type="radio" name="internis" value="Unpaid" id="internis_unpaid" />
                                        <img /> Unpaid
                                    </label>
                                    <label title="Depends on Skill">
                                        <input type="radio" name="internis" value="Depends on Skill" id="internis_dos" />
                                        <img /> Depends on Skill
                                    </label>
                                    <label title="Volunteer">
                                        <input type="radio" name="internis" value="Volunteer" id="internis_volunteer" />
                                        <img /> Volunteer
                                    </label>
                                    <label title="Volunteer">
                                        <input type="radio" name="internis" value="Mentorship" id="internis_mentorship" />
                                        <img /> Mentorship<sup data-toggle="tooltip" data-placement="right" title="Click to show help" class="help-mentorship" onClick="showHelp(this.className)"></sup>
                                    </label>
                                    <label title="Other">
                                        <input type="radio" name="internis" value="Other" id="internis_other"/>
                                        <img /> Other
                                        <div style="display: inline-block;">
                                            <input type="text" class="form-control" name="internis_other" id="internis_other_what" placeholder="Please Specify" required>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="pure-g">
                            <div class="pure-u-1 pure-u-md-1-2">
                                <div class="form-group">
                                    <label for="Opening" class="requiredfield">Openings:</label>
                                    <input type="text" class="form-control" id="opening" name="opening" placeholder="No of available positions" required>
                                </div>
                            </div>
                            <div class="pure-u-1 pure-u-md-1-2">
                                <div class="form-group">
                                    <label for="Deadline" class="requiredfield">Deadline</label>
                                    <input type="date" class="form-control" id="deadline" name="deadline" placeholder="Deadline date" required>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="Intern Location" class="requiredfield">Location</label>
                            <input type="text" class="form-control" id="internlocation" name="internlocation" placeholder="Where will the internship take place?" required>
                        </div>
                        <div class="form-group">
                            <label for="qualification">Preferred qualification(if any):</label>
                            <textarea class="form-control" id="qualification" name="qualification" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="experience">Preferred experience(if any):</label>
                            <textarea class="form-control" id="experience" name="experience" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="skills">Skills(if needed):</label>
                            <textarea class="form-control" id="skills" name="skills" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="responsibility" class="requiredfield">Internship responsibility:</label>
                            <textarea class="form-control" id="responsibility" name="responsibility" rows="3" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="apply" class="requiredfield">How to apply:</label>
                            <textarea class="form-control" id="apply" name="apply" rows="3"
                            Placeholder="Send CV via Email / Contact us on this number/ etc" required></textarea>
                        </div>
                        
                    </fieldset>
                    <div style="width: 100%; text-align: center; margin-top: 10px;" name="submitdiv">
                        <input type="submit"  name="submit" style="background-color:#1d2d44;border-color:rgba(90,41,125,1); width: 10em;" class="btn btn-success btn-send" value="Submit Form">
                    </div>
                </form>
            </div>
        </div>
    </div>