<div class="pure-g serviceswrapper">
	<div class="pure-u-1 serviceblock">
		<div class="pure-g">
			<div class="pure-u-1">
				<div class="pure-g">
					<div class="pure-u-1 servicetitle">
					Introduction
					</div>
					<div class="pure-u-1 servicesubtitle">
					Get to know us ;)
					</div>
				</div>
			</div>
			<div class="pure-u-1">
			InternNepal is Nepal's first internet based portal that delivers smart-recruitment for internships.
			We provide different set of services for students and businesses. Our goal is to deliver the right people to your business and introduce the right business to opportunity seekers.
			For interns-to-be, this a platform to look for internship opportunities. Our listings provide the most latest openings from top institutions to obscure startups. Whether your thing is work modules in big companies or, going	big in small startups, your desired match will be met here.
			</div>
		</div>
	</div>
	<div class="pure-u-1 serviceblock">
	<div class="pure-g">
			<div class="pure-u-1">
				<div class="pure-g">
					<div class="pure-u-1 servicetitle">
					Place your company
					</div>
					<div class="pure-u-1 servicesubtitle">
					Let us feature you. Absolutely no cost
					</div>
				</div>
			</div>
			<div class="pure-u-1">
			Does your firm have an open position for academic/non-acadmic internship? We put a beacon on your back so that the people you're interested in can find you in a snap. Drop a query, fill a form and, once you're listed, your company will gain visibility of thousands of visitors seeking internship oppotunities.
			</div>
		</div>
	</div>
	<div class="pure-u-1 serviceblock">
	<div class="pure-g">
			<div class="pure-u-1">
				<div class="pure-g">
					<div class="pure-u-1 servicetitle">
					Banner Advertisement
					</div>
					<div class="pure-u-1 servicesubtitle">
					Get <span>FREE*</span>, while it lasts!
					</div>
				</div>
			</div>
			<div class="pure-u-1">
			We offer banner advertisement at InternNepal. Stand out from the crowd with a visually appealing banner absolutely FREE of cost. Our banners come in standard 468x60, 728x90, 336x280, 300x250, 160x600, 120x90 sizes. Custom sizes are also available on demand. Drop a query for further details.
			</div>
		</div>
	</div>
	<div class="pure-u-1 serviceblock">
	<div class="pure-g">
			<div class="pure-u-1">
				<div class="pure-g">
					<div class="pure-u-1 servicetitle">
					Intership Training Institute
					</div>
					<div class="pure-u-1 servicesubtitle">
					that extra edge you <em>need</em> to succeed
					</div>
				</div>
			</div>
			<div class="pure-u-1">
			Getting a competitive edge in an internship is determined by the skills you have. Expand your skillset with value-oriented training that'll make you 100% ready for an intership of your choosing. Whether you are interested in IT, Management or Social work, our tie-ups ensure that you get to the best place for	the most effective learning experience.
			</div>
		</div>
	</div>
</div>
