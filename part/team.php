<link href=" //fonts.googleapis.com/css?family=Raleway" rel="stylesheet" type="text/css">
	<div class="pure-g container-class"><!-- serviceswrapper -->
		<div class="pure-u-1 container-head"><!-- serviceblock -->
			<div class="pure-g">
				<div class="pure-u-1 servicetitle">
				The Team
				</div>
				<div class="pure-u-1 servicesubtitle">
				Get to know us ;)
				</div>
			</div>
		</div>
		<div class="pure-u-1 pure-u-md-1-2 profile-container">
			<div class="pure-g">
				<div class="pure-u-1">
					<img class="pure-img" src="images/developers/pratik_200x200.png">
				</div>
				<div class="pure-u-1">
					<p>Pratik Gautam</p>
				</div>
				<div class="pure-u-1">
					<p class="post">Team Leader</p>
				</div>
				<div class="pure-u-1">
					<p class="email">pratik@internnepal.com</p>
				</div>
			</div>
		</div>
		<div class="pure-u-1 pure-u-md-1-2 profile-container">
			<div class="pure-g">
				<div class="pure-u-1">
					<img class="pure-img" src="images/developers/yub.png">
				</div>
				<div class="pure-u-1">
					<p>Yub Raj Basnet</p>
				</div>
				<div class="pure-u-1">
					<p class="post">Web Developer / Designer</p>
				</div>
				<div class="pure-u-1">
					<p class="email">yubraj@internnepal.com</p>
				</div>
			</div>
		</div>
		<div class="pure-u-1 pure-u-md-1-2 profile-container">
			<div class="pure-g">
				<div class="pure-u-1">
					<img class="pure-img" src="images/developers/ron_200x200.png">
				</div>
				<div class="pure-u-1">
					<p>Ronak Agrawal</p>
				</div>
				<div class="pure-u-1">
					<p class="post">Marketing Representative</p>
				</div>
				<div class="pure-u-1">
					<p class="email">ronak@internnepal.com</p>
				</div>
			</div>
		</div>
		<div class="pure-u-1 pure-u-md-1-2 profile-container">
			<div class="pure-g">
				<div class="pure-u-1">
					<img class="pure-img" src="images/developers/binod_200x200.png">
				</div>
				<div class="pure-u-1">
					<p>Binod Paneru</p>
				</div>
				<div class="pure-u-1">
					<p class="post">Marketing Representative</p>
				</div>
				<div class="pure-u-1">
					<p class="email">binod@internnepal.com</p>
				</div>
			</div>
		</div>
		<div class="pure-u-1 pure-u-md-1-2 profile-container">
			<div class="pure-g">
				<div class="pure-u-1">
					<img class="pure-img" src="images/developers/sid_200x200.png">
				</div>
				<div class="pure-u-1">
					<p>Siddhant Rimal</p>
				</div>
				<div class="pure-u-1">
					<p class="post">Web Developer / UI Designer</p>
				</div>
				<div class="pure-u-1">
					<p class="email">siddhant@internnepal.com</p>
				</div>
			</div>
		</div>
	</div>