<!--Navbar-->
<?php switch($pagename){
	
	case "index.php":
	echo '
		<script>
			$(document).ready(function(){
				$("#home").addClass("active");
			});
		</script>
	';
	break;

	case "it.php";
	echo '
		<script>
			$(document).ready(function(){
				$("#it").addClass("active");
			});
		</script>
	';
	break;

	case "socialwork.php";
	echo '
		<script>
			$(document).ready(function(){
				$("#socialwork").addClass("active");
			});
		</script>
	';
	break;

	case "hotel_admin.php";
	echo '
		<script>
			$(document).ready(function(){
				$("#hotel_admin").addClass("active");
			});
		</script>
	';
	break;

	case "request.php";
	echo '
		<script>
			$(document).ready(function(){
				$("#request").addClass("active");
			});
		</script>
	';
	break;

	case "services.php";
	echo '
		<script>
			$(document).ready(function(){
				$("#services").addClass("active");
			});
		</script>
	';
	break;
}
?>

<nav class="navbar fixed-top navbar-toggleable-lg navbar-light bg-faded shrink">
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
	</button>
	<a class="navbar-brand" href="index.php" style="margin-left:3vw;"><img src="images/logo.svg" style="height:70px;"></a>
	<div class="collapse navbar-collapse  justify-content-end" id="navbarNav">
		<ul class="navbar-nav" style="margin-right:100px;">
			<li class="nav-item" id="home">
				<a class="nav-link"  href="index.php" id="homelink">Home <span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Course Specific</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
					<a class="dropdown-item" href="index.php?coursespecific=it" id="it">IT/ComputerScience</a>
					<a class="dropdown-item" href="index.php?coursespecific=social" id="socialwork">Social Work</a>
					<a class="dropdown-item" href="index.php?coursespecific=hotel" id="hotel">Hotel</a>
					<a class="dropdown-item" href="index.php?coursespecific=admin" id="administration">Administration</a>
					<a class="dropdown-item" href="index.php?coursespecific=other" id="other">Miscellaneous</a>
					<hr/>
					<a class="dropdown-item" href="training_institute.php" id="traininginstitute">Training Institute</a>
				</div>
			</li>
			<li class="nav-item" id="request">
				<a class="nav-link" href="request.php"><span class="fa-stack fa-fw fa-lg"  style="font-size: .5em; margin:-.5em 0 0 .2em;"><i class="fa fa-square fa-stack-2x"></i><i class="fa fa-hand-lizard-o fa-stack-1x fa-inverse" ></i>
</span><!-- <i class="fa fa-fw fa-hand-lizard-o"></i> --> Request</a>
			</li>
			<li class="nav-item" id="services">
				<a class="nav-link" href="services.php">Services</a>
			</li>
			<li class="nav-item" id="postaninternship">
				<a class="nav-link" href="internform.php">Post an Internship</a>
			</li>
			<li class="nav-item" id="contact">
				<a class="nav-link" data-target="#contactus" role="button" data-toggle="modal">Contact Us</a>
			</li>
		</ul>
	</div>
</nav>
<?php include 'part/contactmodal.php' ?>
