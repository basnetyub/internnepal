<?php include 'part/config_database.php' ?>
<?php

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$id=$_GET['id'];

if(is_numeric($id) && $id>0){
$sql = "select c.c_name, c.ctype, c.location, c.email, c.contact, c.website, i.intern_title, i.opening, i.internship_category, i.responsibility, i.internis, i.qualification, i.experience, i.skills, i.internlocation, i.deadline, i.apply, c.c_verified, i.intern_id
from company as c
inner join internship as i on c.c_id=i.c_id where i.intern_id='".$id."';";
}
else{
$sql = "select c.c_name, c.ctype, c.location, c.email, c.contact, c.website, i.intern_title, i.opening, i.internship_category, i.responsibility, i.internis, i.qualification, i.experience, i.skills, i.internlocation, i.deadline, i.apply, c.c_verified,i.intern_id
from company as c
inner join internship as i on c.c_id=i.c_id where i.intern_id=1;";
}
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
    	$c_name = $row["c_name"];
		$ctype = handleFullyOther($row["ctype"]);
		$location = $row["location"];
		$email = handleNull($row["email"]);
		$contact = $row["contact"];
		$website = $row["website"];
		$intern_title = $row["intern_title"];
		$opening = $row["opening"];
		$internship_category = handleFullyOther($row["internship_category"]);
		$responsibility = $row["responsibility"];
		$internis = handleFullyOther($row["internis"]);
		$qualification = handleNull($row["qualification"]);
		$experience = handleNull($row["experience"]);
		$skills = handleNull($row["skills"]);
		$internlocation = $row["internlocation"];
		$internship_type = $row["internship_type"];
		$deadline = $row["deadline"];
		$apply = $row["apply"];
		$remain = days_until($deadline);
		$verified = 0;
		if(strcmp($row['c_verified'],"1")==0){
			$verified = 1;
		}
        $intern_id = $row["intern_id"];
        $count = 0;
        while($count<count($hotinternships)){
            if($hotinternships[$count]==$intern_id){
                $hotinternshipflag = 1;  
            }
            $count=$count+1;
        }
    }
} else {
    echo "0 results";
}
$conn->close();
?>

<div class="pure-g interndescription">
    <div class="pure-u-1 pure-u-sm-1-1 pure-u-md-1-2 pure-u-xl-1-2"><p>
    	<!-- Company Overview -->
    	<div class="pure-g descriptionheader">
    		<div class="pure-u-1"><i class="fa fa-fw fa-building-o" aria-hidden="true" style="font-size: 1.5em;"></i>Company Overview</div>
    	</div>
    	<div class="pure-g">
    		<div class="pure-u-6-24">Name:</div>
    		<div class="pure-u-18-24"><!-- {Db Company Name Here} -->
            <?php 
            echo $c_name; 
            if($verified){
              echo '<span id="iscompanyverifieddesc" data-toggle="tooltip" data-placement="right" title="Verified by InternNepal" class="help-verification" onClick="showHelp(this.className)"></span>';  
            } 
            if($hotinternshipflag){
                echo '<span id="internshipishot" data-toggle="tooltip" data-placement="right" title="Hot! Recently Posted" class="help-hotinternship" onClick="showHelp(this.className)"></span>';
            }?>
            </div>
    		<div class="pure-u-6-24">Type:</div>
    		<div class="pure-u-18-24"><!-- {Db Company Type Here} --><?php echo $ctype; ?></div>
    		<div class="pure-u-6-24">Location:</div>
    		<div class="pure-u-18-24"><!-- {Db Company Location Here} --><?php echo $location; ?></div>
    		<div class="pure-u-6-24">Email:</div>
    		<div class="pure-u-18-24"><!-- {Db Company Email Here} --><?php echo $email; ?></div>
    		<div class="pure-u-6-24">Contact No:</div>
    		<div class="pure-u-18-24"><!-- {Db Company Contact Number Here} --><?php echo $contact; ?></div>
    		<div class="pure-u-6-24">Website:</div>
    		<div class="pure-u-18-24"><!-- {Db Company Website Here} --><?php if ($website==NULL){echo 'N/A';}else{echo '<a href="https:	\\\\'.$website.'" target="_blank" rel="nofollow">'.$website.'</a>';} ?></div>
    	</div>
    </p></div>
    <div class="pure-u-1 pure-u-sm-1-1 pure-u-md-1-2 pure-u-xl-1-2"><p>
		<!-- Application Deadline -->
		<div class="pure-g descriptionheader deadline">
    		<div class="pure-u-1"> <i class="fa fa-fw fa-clock-o" aria-hidden="true" style="font-size: 1.5em;"></i>Application Deadline</div>
    	</div>
    	<div class="pure-g">
    		<div class="pure-u-6-24">Should apply by:</div>
    		<div class="pure-u-18-24"><!-- {Db Application Deadline Here} --><?php echo $deadline; ?></div>
    		<div class="pure-u-6-24">Days Remain:</div>
    		<div class="pure-u-18-24"><!-- {Calculate Remaining Days Here} --><?php if($remain<0){echo '<b style="color:red;">EXPIRED '.($remain*-1).' day(s) ago</b>';}else{echo $remain;} ?></div>
    	</div>
    	<div class="pure-g descriptionheader">
    		<div class="pure-u-1"><i class="fa fa-fw fa-info-circle" aria-hidden="true" style="font-size: 1.5em;"></i>How to Apply</div>
    	</div>
    	<div class="pure-g">
    		<div class="pure-u-1 solodescription">
    			<?php echo $apply; ?>
    		</div>
    	</div>
    </p></div>
    <div class="pure-u-1 pure-u-sm-1-1 pure-u-md-1 pure-u-xl-1-2"><p>
    	<!-- Internship Description -->
    	<div class="pure-g descriptionheader">
    		<div class="pure-u-1"><i class="fa fa-fw fa-list-alt" aria-hidden="true" style="font-size: 1.5em;"></i> Internship Description</div>
    	</div>
    	<div class="pure-g">
    		<div class="pure-u-6-24">Title:</div>
    		<div class="pure-u-18-24"><!-- {Db Internship Title Here} --><?php echo $intern_title; ?></div>
    		<div class="pure-u-6-24">Openings:</div>
    		<div class="pure-u-18-24"><!-- {Db Internship Title Here} --><?php echo $opening; ?></div>
    		<div class="pure-u-6-24">Category:</div>
    		<div class="pure-u-18-24"><!-- {Db Internship Category Here} --><?php echo $internship_category; ?></div>
    		<div class="pure-u-6-24">Responsibilities:</div>
    		<div class="pure-u-18-24"><!-- {Db Internship Responsibilities Here} --><?php echo $responsibility; ?></div>
    		<div class="pure-u-6-24">Intern is:</div>
    		<div class="pure-u-18-24"><!-- {Db Internship is Volunteer/Paid/Unpaid/Depends Here} --><?php echo $internis; if(strcmp($internis,"Mentorship")==0){
                                              echo '<sup class="help-mentorship" onClick="showHelp(this.className)"></sup>';  
                                            }?></div>
    		<div class="pure-u-6-24">Qualifications:</div>
    		<div class="pure-u-18-24"><!-- {Db Internship Qualifications Required Here} --><?php echo $qualification; ?></div>
    		<div class="pure-u-6-24">Experience:</div>
    		<div class="pure-u-18-24"><!-- {Db Internship Experience Here} --><?php echo $experience; ?></div>
    		<div class="pure-u-6-24">Skills:</div>
    		<div class="pure-u-18-24"><!-- {Db Internship Skills Here} --><?php echo $skills; ?></div>
    		<div class="pure-u-6-24">Location:</div>
    		<div class="pure-u-18-24"><!-- {Db Internship Location	 Here} --><?php echo $internlocation; ?></div>
    	</div>
    </p></div>
</div>