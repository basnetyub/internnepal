<!-- SWAL -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/sweetalert2/6.5.6/sweetalert2.min.css">
<!-- purecss -->
<link rel="stylesheet" href="https://unpkg.com/purecss@0.6.2/build/pure-min.css" integrity="sha384-UQiGfs9ICog+LwheBSRCt1o5cbyKIHbwjWscjemyBMT9YCUMZffs6UqUTd0hObXD" crossorigin="anonymous">
<link rel="stylesheet" href="https://unpkg.com/purecss@0.6.2/build/grids-responsive-min.css">
<!-- custom-->
<link rel="stylesheet" type="text/css" href="css/ExperimentalCustom.css">
<!-- SWAL -->
<script src="https://cdn.jsdelivr.net/sweetalert2/6.5.6/sweetalert2.min.js"></script>
<style type="text/css">
	.prenav{
		background-color: #eeeeff;
		height:4.375em;
		 vertical-align: middle!important;
		 visibility: visible;
		 opacity:1;
		 line-height: 60px;
	}
	.prenav img{
		height:4.250em;
		visibility: visible;
	}
	.badli.prenav{
		visibility: hidden;
  		opacity: 0;
  		transition: visibility 0s, opacity 0.5s linear;
	}
	.prenav div{
		float:right;
		text-transform: uppercase;
		margin-right:10px;
		font-family: monospace;
		font-size: 1.5em;
		color:rgba(90,41,125,.7);
		transition: .5s;
		overflow: hidden;
		height:70px;
	}
	.fixed-top {
    position: fixed;
    top: 4.375em;
    right: 0;
    left: 0;
    z-index: 1030;
	}
	.nav-link{
		padding:1.2em;
	}
	.badli .nav-link{
		padding:.8em;
		transition: 0.5s;
	}
	.hamburgermenu{
		color:#eeeeff;
		font-size: 1.8em;
	}
	.navbar-toggler{
		margin:.35em 0 0 0;
		text-decoration: none;
		padding:0 .45em 0 .45em;
	}
	a.navbar-brand{
		padding: 0px!important;
	}
	.navbar-light .navbar-toggler
	{
		border-color: rgba(0,0,0,0);
	}
	.navbar-light .navbar-brand:focus, .navbar-light .navbar-brand:hover, .navbar-light .navbar-toggler:focus, .navbar-light .navbar-toggler:hover {
    color: rgba(0,0,0,0);
    text-decoration: none;
}
	.nav-item{
		padding:0px 10px 0px 10px!important;
		/*background-color: rgba(90,41,125,1);*/
		color: #eeeeff!important;
	}
	nav{
			box-shadow:0px 5px 15px rgba(90,41,125,0)!important;
			background-color: rgba(90,41,125,1)!important;
			min-height:60px;/*maintains deflation in mobile view*/
			/*height:3.750em;*/	
			padding:0px;	
	}
	.navbar{
		padding: 0px!important;
	}
	.active{
		background-color: #eeeeff;
	}
	nav ul li{
		-webkit-transition: 0.5s;
		-moz-transition: 0.5s;
		-ms-transition: 0.5s;
		transition: 0.5s;
		padding: 0px;
	}
	.navbar-brand{
		/*margin:-12px 0 0 -20px;*/
	}
	.navbar-brand img {
		-webkit-transition: 0.5s;
		-moz-transition: 0.5s;
		max-height: 3.750em;
		-ms-transition: 0.5s;
		transition: 0.5s;
		margin:-200px 0 0 -200px;
	}
	.navbar{
		-webkit-transition: 0.5s;
		-moz-transition: 0.5s;
		-ms-transition: 0.5s;
		transition: 0.5s;
		/*dont change height here. affects mobile view*/
	}
	
	.badli {
		-webkit-transition: 0.5s;
		-moz-transition: 0.5s;
		-ms-transition: 0.5s;
		transition: 0.5s;
	}
	.badli ul li{
		padding:0px;
		-webkit-transition: 0.5s;
		-moz-transition: 0.5s;
		-ms-transition: 0.5s;
		transition: 0.5s;
	}
	.badli .navbar-brand img {
		max-height: 50px;
		-webkit-transition: 0.5s;
		-moz-transition: 0.5s;
		-ms-transition: 0.5s;
		transition: 0.4s;
		margin:0px 0 0 0px;
	}
	.badli .navbar-band{
		margin:0px 0 0 0px;
	}
	nav.badli{
	box-shadow:0px 5px 15px rgba(90,41,125,1)!important;
	background-color: rgba(90,41,125,.9)!important;
	min-height:50px;
	}
	div.dropdown-menu{
		visibility: hidden;
  		opacity: 0;
  		transition: visibility 0s, opacity 1.5s linear;
	}
	.show>.dropdown-menu{
	
		/*transition: 1.5s;*/
		/*transition: visibility 1.5s, opacity 1.5s linear;*/
		visibility: visible;
  		opacity: 1;
	}
	.badli.fixed-top{
    position: fixed;
    top: 0px!important;
    right: 0;
    left: 0;
    z-index: 1030;
    transition: 0.5s;
	}
	
</style>
<script type="text/javascript">
	
		$(window).scroll(function() {
			if ($(document).scrollTop() > 0) {
			$('.navbar').addClass('badli');
			$('.prenav').addClass('badli');
			}
			else {
			$('.navbar').removeClass('badli');
			$('.prenav').removeClass('badli'); }
			});
</script>
<!--Navbar-->
<?php switch($pagename){
	case "experimental.php":
?>
<script>
	$(document).ready(function(){
$("#home").addClass("active");
	});
</script>

<?php break;
	case "it.php";
?>
<script>
	$(document).ready(function(){
	$("#it").addClass("active");
	});
</script>

<?php break;
	case "socialwork.php";
?>
<script>
	$(document).ready(function(){
	$("#socialwork").addClass("active");
	});
</script>

<?php break;
	case "hotel_admin.php";
?>
<script>
	$(document).ready(function(){
	$("#hotel_admin").addClass("active");
	});
</script>

<?php break;
	case "request.php":
?>
<script>
	$(document).ready(function(){
	$("#request").addClass("active");
	});
</script>

<?php break;
	case "contact.php":
?>
<script>
	$(document).ready(function(){
	$("#contact").addClass("active");
	});
</script>

<?php break;
case "services.php";
?>
<script>
	$(document).ready(function(){
	$("#services").addClass("active");
	});
</script>
<?php } ?>

<div class="prenav">
	<img src="images/logo.svg">
	<div>
	Nepal'<span style="text-transform: lowercase;">s</span> #1 Internship-seeking niche portal
	</div>
</div>
<nav class="navbar fixed-top navbar-toggleable-md navbar-light bg-faded">
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	<!-- <span class="navbar-toggler-icon"></span> --><i class="fa fa-bars hamburgermenu" aria-hidden="true"></i>
	</button>
	<a class="navbar-brand" href="index.php"><img src="images/logo.svg" style="height:50px;"></a>
	<div class="collapse navbar-collapse  justify-content-end" id="navbarNav">
		<ul class="navbar-nav" style="margin-right:100px;">
			<li class="nav-item" id="home">
				<a class="nav-link"  href="index.php"><i class="fa fa-fw fa-home"></i>Home <span class="sr-only">(current)</span></a>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-fw fa-book"></i>
					Course Specific
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
					<a class="dropdown-item" href="it.php" id="it"><i class="fa fa-fw fa-desktop"></i>IT/ComputerScience</a>
					<a class="dropdown-item" href="socialwork.php" id="socialwork"><i class="fa fa-fw fa-handshake-o"></i>Social Work</a>
					<a class="dropdown-item" href="hotel_admin.php" id="hotel_admin"><i class="fa fa-fw fa-h-square"></i>Hotel/Administration</a>
				</div>
			</li>
			<li class="nav-item" id="request">
				<a class="nav-link" href="request.php"><i class="fa fa-fw fa-folder"></i>Request</a>
			</li>
			<li class="nav-item" id="services">
				<a class="nav-link" href="services.php"><i class="fa fa-fw fa-file-o"></i>Services</a>
			</li>
			<li class="nav-item" id="contact">
				<a class="nav-link" data-target="#contactus" role="button" data-toggle="modal"><i class="fa fa-fw fa-phone-square"></i>Contact Us</a>
			</li>
		</ul>
	</div>
</nav>
<?php include 'contactmodal.php' ?>
