<!-- OFFLINE VERSION -->

<!-- jQuery 3.2.1 -->
<script 
	src="../localAwesome/LOCAL/jQuery/jquery-3.2.1.min.js">	
</script>
<!-- <script src="../localAwesome/LOCAL/jQuery/jquery-3.1.1.slim.min.js"></script> -->
<!-- tether -->
<script src="../localAwesome/LOCAL/tether/1.4.0/js/tether.min.js"></script>
<script src="../localAwesome/LOCAL/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<!-- SWAL -->
<script src="../localAwesome/LOCAL/SWAL2/sweetalert2.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {  
    
    // submit form using $.ajax() method
    
    $('#contactmodalform').submit(function(e){
        
        e.preventDefault(); // Prevent Default Submission
        $.ajax({
            url: 'part/sendmail.php',
            type: 'POST',
            data: $(this).serialize() // it will serialize the form data
        })
        .done(function(data){
            $('#contactmodalformbody').fadeOut('slow', function(){
                $('#contactmodalformbody').fadeIn('slow').html(data);
            });
        })
        .fail(function(){
            swal('Ajax Error!',
                '<p style="line-height:2em;">Something is wrong at <em>your</em> end! Try again later or report this issue at <code>feedback@internnepal.com</code></p>',
                'error')
        });
    });
    

});
</script>
<script type="text/javascript">
function showHelp(helptarget){
    if(helptarget=="help-traininginstitute"){
        swal('Training Institute', 'Training institutes are educational facilities that provide specialized skill-oriented internship-preparation training, giving you that extra edge to outcompete other candidates!', 'info')
    }
}
</script>