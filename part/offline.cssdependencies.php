<!-- OFFLINE VERSION -->

<link rel="stylesheet" href="../localAwesome/LOCAL/font-awesome/4.7.0/css/font-awesome.min.css">
<link href=" //fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
<!--Bootstrap 4 CDN-->
<link rel="stylesheet" href="../localAwesome/LOCAL/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
<!-- SWAL -->
<link rel="stylesheet" href="../localAwesome/LOCAL/SWAL2/sweetalert2.min.css">
<!-- purecss -->
<link rel="stylesheet" href="../localAwesome/LOCAL/pure-release-0.6.2/pure-min.css">
<link rel="stylesheet" href="../localAwesome/LOCAL/pure-release-0.6.2/grids-responsive-min.css">
<!-- custom-->
<link rel="stylesheet" type="text/css" href="css/custom.css">
<link rel="stylesheet" type="text/css" href="css/footer.css">
<link rel="icon" type="image/png" href="images/icon.png">

