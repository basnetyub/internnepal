<!-- jQuery 3.2.1 -->
<script 
	src="https://code.jquery.com/jquery-3.2.1.min.js" 
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous">	
</script>
<!-- <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script> -->
<!-- tether -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<!-- SWAL -->
<script src="https://cdn.jsdelivr.net/sweetalert2/6.5.6/sweetalert2.min.js"></script>
<!-- custom -->
<script src="javascript/helptext.js"></script>

<script type="text/javascript">
$(document).ready(function() {  
//--------------------------------------------- 
    // submit form using $.ajax() method
    $('#contactmodalform').submit(function(e){
        
        e.preventDefault(); // Prevent Default Submission
        $.ajax({
            url: 'part/sendmail.php',
            type: 'POST',
            data: $(this).serialize() // it will serialize the form data
        })
        .done(function(data){
            $('#contactmodalformbody').fadeOut('slow', function(){
                $('#contactmodalformbody').fadeIn('slow').html(data);
            });
        })
        .fail(function(){
            showHelp('help-ajaxerror')
        });
    });
//---------------------------------------------
    //initializing tooltips
    $('[data-toggle="tooltip"]').tooltip();

//---------------------------------------------


});
</script>