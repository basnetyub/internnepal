<?php include 'config_database.php' ?>
<?php
	$fname=$_POST['fname'];
	$lname=$_POST['lname'];
	$email=$_POST['email'];
	$phone=$_POST['phone'];
	$faculty=$_POST['faculty'];
	$seeking=$_POST['seeking'];
	$message=$_POST['message'];

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$executereqstat=0;
//-------------------------
//PHASE-1
$sql = "INSERT INTO request_std (req_fname,req_lname,req_email,req_phone,req_faculty,req_seeking,req_message) 
values('".$_POST['fname']."',
    '".$_POST['lname']."','".$_POST['email']."','".$_POST['phone']."','".$_POST['faculty']."','".$_POST['seeking']."','".$_POST['message']."')";
if($result = $conn->query($sql)){
    $executereqstat=$executereqstat+1;
}
//-------------------------

if($executereqstat>0){
    echo '
    <script>
        showHelp(\'alert-request-success\')
    </script>
        <table class="table table-striped" border="0">
            <tr>
                <td colspan="2">
                    <div class="alert alert-info">
                        <strong>Thank You for sending your request.</strong> We have received the following information form you.<br/>Please <a data-target="#contactus" role="button" data-toggle="modal"><b>contact us</b></a> if you find any discrepancies in your information.
                    </div>
                </td>
            </tr>
            <tr>
                <td>First Name</td>
                <td>'.$fname.'</td>
            </tr>
            <tr>
                <td>Last Name</td>
                <td>'.$lname.'</td>
            </tr>
            <tr>
                <td>Your email</td>
                <td>'.$email.'</td>
            </tr>
            <tr>
                <td>Contact No.</td>
                <td>'.$phone.'</td>
            </tr>
            <tr>
                <td>Faculty</td>
                <td>'.$faculty.'</td>
            </tr>
            <tr>
                <td>Seeking</td>
                <td>'.$seeking.'</td>
            </tr>
            <tr>
                <td>Message</td>
                <td>'.$message.'</td>
            </tr>
        </table>
    ';
}
else{
    echo '
    <script>
        showHelp(\'alert-request-error\')
    </script>
        <div class="alert alert-danger">
            <strong>Error!</strong>, Please <a href="request.php"> try again </a>
        </div>
    ';
}

?>
