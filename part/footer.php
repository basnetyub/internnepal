<footer class="footer-distributed">
	<div class="footer-left">
	<div>
		<a href="index.php"> <img src="images/logo.svg"> </a>

		<p class="footer-company-name"> <!-- &nbsp Intern Nepal --></p>
		<a href="our_team.php" style="font-size:.9em;">About the Team</a>
		<!-- <ul style="margin: 1em 0 0 -2em;">Useful Links:
		<li style="margin-left: 1em;"><a href="internform.php">Post an Internship</a></li>
		<li style="margin-left: 1em;"><a href="training_institute.php">Training Institutes</a></li>
		</ul> -->
	</div>
	</div>
	<div class="footer-center">
		<div>
			<i class="fa fa-map-marker"></i>
			<p><span><!-- Baneshwor --> </span>Kathmandu, Nepal</p>
		</div>
		<div>
			<i class="fa fa-phone"></i>
			<p>9843396403, 9843386239</p>
		</div>
		<div>
			<i class="fa fa-envelope"></i>
			<p>contactus@internnepal.com</p>
		</div>
	</div>
	<div class="footer-right">
		<div>
		<p class="footer-company-about">
			<a href="services.php"> <span>About Us</span> </a>
			We are Intern Nepal, a service-first internship-seeking portal, with the help of which you can find internship opportunities that fulfill your academic or non academic requirements. Our speciality lies in every faculty of study.
		</p>
		</div>
		<div class="footer-icons">
			<a href="https://www.facebook.com/Intern-Nepal-1622316597793639/" target="_blank"><i class="fa fa-facebook"></i></a>
			<a href="https://www.youtube.com/channel/UC5FX0zMcpvSaLQ9lraR5I5Q" target="_blank"><i class="fa fa-youtube"></i></a>
			<a href="https://twitter.com/internnepal" target="_blank"><i class="fa fa-twitter" target="_blank"></i></a>
			<a href="https://www.instagram.com/official.internnepal" target="_blank"><i class="fa fa-instagram"></i></a>
			<a href="https://plus.google.com/116550689373393085882" target="_blank"><i class="fa fa-google-plus"></i></a>
		</div>
	</div>
	<div class="pure-g footerannotationbar">
		<div class="pure-u-1">
			<div class="pure-g">
				<div class="pure-u-1 pure-u-sm-9-24 pure-u-md-8-24 pad">
					&nbsp;
				</div>
				<div class="pure-u-1 pure-u-sm-6-24 copyright">
					&copy;<?php if(date('Y')>2017){echo '2017-'.date('Y');}else{echo date('Y');} ?> Intern Nepal 
				</div>
				<div class="pure-u-1 pure-u-sm-4-24 pad">
					&nbsp;
				</div>
				<div class="pure-u-1 pure-u-md-6-24 links">
				<a href="services.php">Services</a> | <a href="privacypolicy.php">Privacy Policy</a> | <a href="sitemap.php">Sitemap</a>
				</div>
			</div>
		</div>
	</div>
</footer>