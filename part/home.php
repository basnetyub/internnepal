                    <!-- 60% -->
                    <div class="pure-g">
                        <div class="pure-u-1 grid-label labelBackdropDesktop hideScrambledEggs">#Available</div>
                    </div>
                    <div class="stickyheader">
                    <div class="pure-g hideScrambledEggs">
                        <div class="pure-u-1 pure-u-sm-1-4 grid-header">
                            <p>Company</p>
                        </div>
                        <!-- 3 -->
                        <div class="pure-u-1 pure-u-sm-1-6 grid-header">
                            <p>Internship Type</p>
                        </div>
                        <!-- 2 -->
                        <div class="pure-u-1 pure-u-sm-7-24 grid-header">
                            <p>Intern Title</p>
                        </div>
                        <!-- 4 -->
                        <div class="pure-u-1 pure-u-sm-1-6 grid-header">
                            <p>Category</p>
                        </div>
                        <!-- 2 -->
                        <div class="pure-u-1 pure-u-sm-3-24 grid-header">
                            <p id="deadlinetitle">Deadline</p>
                        </div>
                        <!-- 1 -->
                    </div>
                    </div>
                    <!-- Repeat this segment in database to make dynamic listings-->
                    <?php include 'listingtable.php' ?>
