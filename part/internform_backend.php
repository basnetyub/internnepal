<?php include 'config_database.php'/*reference to php in same folder*/ ?>
<?php
//get data from post
//For Company Table
    $cname = $_POST['cname']; //c_name
    $ctype = $_POST['ctype']; //ctype
    //$ctypeis if OTHER selected //ctype
    if(strcmp($ctype,"Other")==0){
    	$ctypeis = $_POST['ctypeother'];
    	$ctype = $ctype." (".$ctypeis.")";
    }
    else{
    	//!Other case: do nothing
    }
    $location = $_POST['location']; //location
    $contact = $_POST['contact']; //contact
    $email = $_POST['email']; //email
    $website = $_POST['website']; //website
    //For Internship Table
    $intern_title = $_POST['intern_title']; //intern_title
    $internship_category = $_POST['internship_category']; //internship_type
    //$internship_category_other if OTHER selected //internship_type
    if(strcmp($internship_category,"Other")==0){
        $internship_category_other = $_POST['internship_category_other'];
        $internship_category = $internship_category." (".$internship_category_other.")";
    }
    else{
        //!Other case: do nothing
    }
    $internis = $_POST['internis']; //internis
    //$internis_other if OTHER selected //internis
    if(strcmp($internis,"Other")==0){
        $internis_other = $_POST['internis_other'];
        $internis = $internis." (".$internis_other.")";
    }
    else{
        //!Other case: do nothing 
    }
    $opening = $_POST['opening']; //opening
    $deadline = $_POST['deadline']; //deadline
    $internlocation = $_POST['internlocation']; //internlocation
    $qualification = $_POST['qualification']; //qualification
    $experience = $_POST['experience']; //experience
    $skills = $_POST['skills']; //skills
    $responsibility = $_POST['responsibility']; //responsibility
    $apply = $_POST['apply']; //apply


// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$executestat=0;
//-------------------------
//PHASE-1
$sql = "INSERT INTO company (c_name,ctype,location,contact,email,website) 
values('".$cname."','".$ctype."','".$location."','".$contact."','".$email."','".$website."')";
if($result = $conn->query($sql)){
    $executestat=$executestat+1;
}
//-------------------------

//-------------------------
//PHASE-2
$max_c_id = $conn->insert_id;
//-------------------------

//-------------------------
//PHASE-3
$sql = "INSERT INTO internship (c_id,intern_title,internis,opening,deadline,internlocation,qualification,experience,skills,responsibility,apply, internship_category) 
values('".$max_c_id."','".$intern_title."','".$internis."','".$opening."','".$deadline."','".$internlocation."','".$qualification."','".$experience."','".$skills."','".$responsibility."','".$apply."','".$internship_category."')";
if($result = $conn->query($sql)){
    $executestat=$executestat+1;
}
//-------------------------

if ($executestat == 2) {
    echo '

    <div class="alert alert-info" id="internform-focus-message" tabindex="1">
    <table class="table table-striped" border="0" style="text-align:center;">
        <tr>
            <th style="text-align:center;">
                    <strong>We have received your form.</strong>
            </th>
        </tr>
        <tr>
            <td>
                We will soon verify and update your information. You will be notifed when it is done.
            </td>
        </tr>
    </table>
    </div>
    <script type="text/javascript">
        showHelp(\'alert-internform-success\');
        $(document).ready(function(){
            $("#internform-focus-message").focus();
        });
    </script>
    ';
}
else{ 
    echo '
    <div class="alert alert-danger" id="internform-focus-message" tabindex="0">
        <strong>Error!</strong>, Please <a href="internform.php"> try again </a>
    </div>
    <script type="text/javascript">
    showHelp(\'alert-internform-error\')
     $(document).ready(function(){
            $("#internform-focus-message").focus();
        });
    </script>
    ';
}
?>