<?php include 'part/config_database.php' ?>
<?php

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$id=$_GET['id'];

if(is_numeric($id) && $id>0){
$sql = "select ti.ti_name,ti.ti_ctype, ti.ti_location, ti.ti_contact, ti.ti_email, ti.ti_website,  ac.ac_title, ti.ti_id 
from training_institute as ti
inner join available_courses as ac on ti.ti_id=ac.ti_id where ti.ti_id='".$id."';";
}
else{
$sql = "select ti.ti_name,ti.ti_ctype, ti.ti_location, ti.ti_contact, ti.ti_email, ti.ti_website,  ac.ac_title, ti.ti_id 
from training_institute as ti
inner join available_courses as ac on ti.ti_id=ac.ti_id where ti.ti_id=1;";
}
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
    	$ti_name = $row["ti_name"];
		$ti_ctype = $row["ti_ctype"];
		$ti_location = $row["ti_location"];
		$ti_email = $row["ti_email"];
		$ti_contact = $row["ti_contact"];
		$ti_website = $row["ti_website"];
		$ac_title = $row["ac_title"];
    }
} else {
    echo "0 results";
}
$conn->close();
?>


<div class="pure-g interndescription">
    <div class="pure-u-1 pure-u-sm-1-1 pure-u-md-1-2 pure-u-xl-1-2"><p>
    	<!-- Training Institute Overview -->
    	<div class="pure-g descriptionheader">
    		<div class="pure-u-1"><i class="fa fa-fw fa-building-o" aria-hidden="true" style="font-size: 1.5em;"></i>Overview of Training Institute</div>
    	</div>
    	<div class="pure-g">
    		<div class="pure-u-6-24">Name:</div>
    		<div class="pure-u-18-24"><!-- {Db Company Name Here} --><?php echo $ti_name; ?></div>
    		<div class="pure-u-6-24">Type:</div>
    		<div class="pure-u-18-24"><!-- {Db Company Type Here} --><?php echo handleFullyOther($ti_ctype); ?></div>
    		<div class="pure-u-6-24">Location:</div>
    		<div class="pure-u-18-24"><!-- {Db Company Location Here} --><?php echo $ti_location; ?></div>
    		<div class="pure-u-6-24">Email:</div>
    		<div class="pure-u-18-24"><!-- {Db Company Email Here} --><?php echo $ti_email; ?></div>
    		<div class="pure-u-6-24">Contact No:</div>
    		<div class="pure-u-18-24"><!-- {Db Company Contact Number Here} --><?php echo $ti_contact; ?></div>
    		<div class="pure-u-6-24">Website:</div>
    		<div class="pure-u-18-24"><!-- {Db Company Website Here} --><?php if ($ti_website==NULL){echo 'N/A';}else{echo '<a href="https:	\\\\'.$ti_website.'" target="_blank" ref="nofollow" >'.$ti_website.'</a>';} ?></div>
    	</div>
    </p></div>
    <div class="pure-u-1 pure-u-sm-1-1 pure-u-md-1-2 pure-u-xl-1-2"><p>
		<!-- List of Courses -->
		<div class="pure-g descriptionheader">
    		<div class="pure-u-1"><i class="fa fa-fw fa-list-alt" aria-hidden="true" style="font-size: 1.5em;"></i> Available Courses</div>
    	</div>
    	<div class="pure-g">
    		<div class="pure-u-1 solodescription">
    			<?php echo $ac_title; ?>
    		</div>
    	</div>
    	
    </p></div>
    <div class="pure-u-1 pure-u-sm-1-1 pure-u-md-1 pure-u-xl-1-2"><p>
    	<!-- How to Apply -->
    	<div class="pure-g descriptionheader">
    		<div class="pure-u-1"><i class="fa fa-fw fa-info-circle" aria-hidden="true" style="font-size: 1.5em;"></i>How to Contact Us</div>
    	</div>
    	<div class="pure-g">
    		<div class="pure-u-1 solodescription">
    			Call <?php echo '<i class="fa fa-phone" aria-hidden="true"></i> '.$ti_contact.' or visit the website ('.$ti_website.')' ?>
    		</div>
    	</div>
    	
    </p></div>
</div>