<div class="pure-g" style="padding:20px 10px 30px 10px; border:1px solid rgba(90,41,125,.1); background-color: #eeeeff;">
<div class="pure-u-1">
<div id="req-form-content">
    <div style="padding-bottom: 10px;">
    <div style="background-color: #1d2d44; margin: -20px -10px 0 -10px; padding:5px 10px 1px 10px; color: #eeeeff;">
    <p class="lead" style=" font-size: 1.2em!important;">Fill the following form with the interest of your intern subject. We'll reply (in your Email Address) when appropriate place is found for you.</p>
    </div></div>
    <!-- <div id="form-content"> -->
    <form id="req-form" autocomplete="off"  method="post">
        <div class="messages"></div>
        <div class="controls">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form_name" class="requiredfield">Firstname</label>
                        <input id="form_name"  type="text" name="fname" class="form-control"  required="required" data-error="Firstname is required.">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                
                
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form_lastname" class="requiredfield">Lastname</label>
                        <input id="form_lastname"  type="text" name="lname" class="form-control" required="required" data-error="Lastname is required.">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form_email" class="requiredfield">Email</label>
                        <input id="form_email"  type="email" name="email" class="form-control" required="required" data-error="Valid email is required.">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form_phone">Phone</label>
                        <input id="form_phone"  type="tel" name="phone" class="form-control" >
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form_name" class="requiredfield">Your faculty</label>
                        <input id="form_name" type="text" name="faculty" class="form-control" placeholder="Eg. BSCCSIT, BIM, BHM, BBA, BASW, BBS, +12... *" required="required" data-error="Faculty is required.">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="form_lastname" class="requiredfield">Seeking for</label>
                        <input id="form_lastname" type="text" name="seeking" class="form-control" placeholder="Intern in hotel/Computer Networking/administration *" required="required" data-error="You must provide areas of your interest.">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="form_message" class="requiredfield">Message</label>
                        <textarea id="form_message" name="message" class="form-control" placeholder="Describe your academics and areas of interest. *" rows="4" required="required" data-error="You must write something."></textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <input type="submit"  name="submit" style="background-color:#1d2d44;" class="btn btn-success btn-send" value="Send message">
                </div>
            </div>
            
        </div>
    </form>
</div>
</div>
</div>

