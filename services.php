<?php include 'part/config_global.php' ?>
<!DOCTYPE html>
<html>
<head>
    <title>Services -- Intern Nepal</title>
    <?php include 'part/global_meta.php' ?>
    <meta name="description" content="Know what services we provide you at Intern Nepal. Get Information about active listings, partnership and advertisement.">
    <?php include 'part/cssdependencies.php' ?>
</head>
<?php include 'part/wrapper_top.php' ?>
       <?php include 'part/serviceslist.php' ?>         
<?php include 'part/wrapper_bottom.php' ?>
<?php include 'part/jsdependencies.php' ?>
</html>