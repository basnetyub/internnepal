<?php include 'part/config_global.php' ?>
<!DOCTYPE html>
<html>
<head>
    <title>Request -- Intern Nepal</title>
    <?php include 'part/global_meta.php' ?>
    <meta name="description" content="Can't find something you're interested in? Send us a Request ad we'll make it happen!">
    <?php include 'part/cssdependencies.php' ?>
</head>
<?php include 'part/wrapper_top.php' ?>
       <?php include 'part/requestform.php' ?>         
<?php include 'part/wrapper_bottom.php' ?>
<?php include 'part/jsdependencies.php' ?>
<script src="javascript/requestform.js"></script>
</html>