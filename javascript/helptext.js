function showHelp(helptarget){
    if(helptarget=="help-traininginstitute"){
        swal('Training Institute', 'Training institutes are educational facilities that provide specialized skill-oriented internship-preparation training, giving you that extra edge to outcompete other candidates!', 'info').catch(swal.noop)
    }
    else if(helptarget=="help-mentorship"){
        swal({title:'Mentorship', 
            html:'Some Internship opportunities are highly sought after. InternNepal marks Mentorships as those internship opportunities where the intern has to pay the company for the internship opportunity.', 
            type:'info',
            allowOutsideClick: false,
            confirmButtonText:'Ok',
            confirmButtonColor: '#1D2D44'
        }).catch(swal.noop)
    }
    else if(helptarget=="help-verification"){
        swal({title:'Verified Company', 
            html:'InternNepal has a standard 2-step approval procedure for companies. Verified companies pass through additional background crosschecks and are <em>guaranteed</em> by <b>InternNepal</b> to be 100% authentic.', 
            type:'success',
            allowOutsideClick: false,
            confirmButtonText:'Got it!',
            confirmButtonColor: '#1D2D44'
        }).catch(swal.noop)
    }
    else if(helptarget=="help-hotinternship"){
        swal({title:'Hot Internships', 
            html:'Recently posted Internships are marked <span id="internshipishot"></span> or hot.', 
            type:'info',
            allowOutsideClick: false,
            confirmButtonText:'Good to know',
            confirmButtonColor: '#1D2D44'
        }).catch(swal.noop)
    }
    else if(helptarget=="help-ajaxerror"){
        swal('Ajax Error!',
                '<p style="line-height:2em;">Something is wrong at <em>your</em> end! Try again later or report this issue at <code>feedback@internnepal.com</code></p>',
                'error').catch(swal.noop)
    }
    else if(helptarget=="alert-contact-success"){
       swal({
          title: 'Done!',
          type: 'success',
          html: '<p style="line-height:2em;">Your mail has been received sucessfully.</p>',
          showConfirmButton: false,
          allowOutsideClick: false,
          timer: 2400
        }).catch(swal.noop)
    }
    else if(helptarget=="alert-contact-error"){
       swal({
            title:'Oops...',
            html:'<p style="line-height:2em;">Something went wrong on our end. Try again later or report this issue at <code>feedback@internnepal.com</code></p>',
            type:'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Try Again',
            cancelButtonText: 'Later',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
            allowOutsideClick:false
        }).catch(swal.noop).then(function () {
            window.location.reload()
            }, function (dismiss) {
                if (dismiss === 'cancel') {
                  swal({
                    title:'Cancelled',
                    html:'<p style="line-height:2em;">You have cancelled filling this form.  Please report this issue at <code>feedback@internnepal.com</code> so that we can know and resolve it. We hope you try again later. :) ',
                    type:'error',
                    allowOutsideClick:false
                  }).catch(swal.noop)
                  $('#contactus').modal('hide')
                }//IF END
            }) // then (FX(1){},FX(2){})
        }  
        else if(helptarget=="alert-internform-success"){
           swal({
              title: 'Success!',
              type: 'success',
              html: 'Your information has been sent',
              showConfirmButton: false,
              showCancelButton: false,
              allowOutsideClick: false,
              timer: 2400
            }).catch(swal.noop)
        }
    else if(helptarget=="alert-internform-error"){
       swal({
          title: 'Oops!',
          type: 'error',
          html: 'Something went wrong. We were not able to receive your information.Please <a href="internform.php"> try again </a>',
          showConfirmButton: false,
          allowOutsideClick: false,
          timer: 2400
        }).catch(swal.noop)
    }
    else if(helptarget=="alert-request-success"){
       swal({
        title:'Sent',
        html:'Your request has been successfully received',
        type:'success',
        allowOutsideClick: false,
        showConfirmButton: false,
        timer: 2400
        }).catch(swal.noop)
    }
    else if(helptarget=="alert-request-error"){
       swal({
          title: 'Oops!',
          type: 'error',
          html: 'Something went wrong. We were not able to receive your request. Please <a href="request.php"> try again </a>',
          showConfirmButton: false,
          allowOutsideClick: false,
          timer: 2400
        }).catch(swal.noop)
    }

}