$(document).ready(function() {
var form_testing = 1; /*Set 1 if testing*/
    /*TRUE for form_testing--*/
      if (form_testing){
      $('form input').hide();
      $('form label').hide();
      $('form textarea').hide();
      $('form legend').hide();
      $('form div').hide();
      $("form legend[name='legend1']").show();
      $("form legend[name='legend1']").html("TestMode: Disable form_testing on internform.js to get normal form");
      $("form div[name='submitdiv']").show();
      $("form input[name='submit']").show();
      $('#cname').prop('disabled',true);
      var rads=document.getElementsByName("ctype");
      for(var i=0; i<rads.length;i++ )
      {
        document.getElementsByName("ctype")[i].disabled = true;
      }
      $('#location').prop('disabled',true);
      $('#internform_contact').prop('disabled',true);
      $('#intern_title').prop('disabled',true);
      var rads=document.getElementsByName("internship_category");
      for(var i=0; i<rads.length;i++ )
      {
        document.getElementsByName("internship_category")[i].disabled = true;
      }
      $('#internis_paid').prop('disabled',true);
      $('#internis_unpaid').prop('disabled',true);
      $('#internis_dos').prop('disabled',true);
      $('#internis_volunteer').prop('disabled',true);
      $('#internis_mentorship').prop('disabled',true);
      $('#internis_other').prop('disabled',true);
      $('#opening').prop('disabled',true);
      $('#deadline').prop('disabled',true);
      $('#internlocation').prop('disabled',true);
      $('#responsibility').prop('disabled',true);
      $('#apply').prop('disabled',true);
    }
    /*--*/

    $('#ctypeotherwhat').prop('disabled',true);
    $('#internship_category_other_what').prop('disabled',true);
    $('#internis_other_what').prop('disabled',true);
    $('input[type="radio"]').on('click', function() {
      if ($("#ctypeother").is(':checked')) {
        $('#ctypeotherwhat').prop('disabled',false);
      } else {
        $('#ctypeotherwhat').prop('disabled',true);
      }
      if ($("#internship_category_other").is(':checked')) {
        $('#internship_category_other_what').prop('disabled',false);
      } else {
        $('#internship_category_other_what').prop('disabled',true);
      }
       if ($("#internis_other").is(':checked')) {
        $('#internis_other_what').prop('disabled',false);
      } else {
        $('#internis_other_what').prop('disabled',true);
      }
    });

    $('#reg-form').submit(function(e){
            e.preventDefault(); // Prevent Default Submission
            $.ajax({
                url: 'part/internform_backend.php',
                type: 'POST',
                data: $(this).serialize() // it will serialize the form data
            })
            .done(function(data){
                $('#form-content').fadeOut('slow', function(){
                    $('#form-content').fadeIn('slow').html(data);
                });
            })
            .fail(function(){
                alert('Error! Please try again.' );
            });
    });
});


