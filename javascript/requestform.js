$(document).ready(function() { 
    $('#req-form').submit(function(e){
        
        e.preventDefault(); // Prevent Default Submission
        $.ajax({
            url: 'part/request_backend.php',
            type: 'POST',
            data: $(this).serialize() // it will serialize the form data
        })
        .done(function(data){
            $('#req-form-content').fadeOut('slow', function(){
                $('#req-form-content').fadeIn('slow').html(data);
            });
        })
        .fail(function(){
            alert('Error! Please try again.' );
    			
        });
    });
});