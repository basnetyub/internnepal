-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2017 at 02:51 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testtt`
--

-- --------------------------------------------------------

--
-- Table structure for table `available_courses`
--

CREATE TABLE `available_courses` (
  `ac_id` int(10) UNSIGNED NOT NULL,
  `ti_id` int(6) DEFAULT NULL,
  `ac_title` varchar(10000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `available_courses`
--

INSERT INTO `available_courses` (`ac_id`, `ti_id`, `ac_title`) VALUES
(1, 1, 'PHP OOP Training'),
(2, 2, 'CCNA, Red Hat, MCSE'),
(3, 3, 'HTML, CSS, JS, DHTML, Bootstrap'),
(4, 4, 'Nepali Cooking, Indian Cuisine');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `c_id` int(10) UNSIGNED NOT NULL,
  `c_name` varchar(10000) DEFAULT NULL,
  `ctype` varchar(1000) DEFAULT NULL,
  `c_approved` int(1) NOT NULL DEFAULT '0' COMMENT 'Boolaean. DEFAULT 0. 1 IF TRUE.',
  `c_verified` int(1) NOT NULL DEFAULT '0' COMMENT 'Boolean. DEFAULT 0. 1 IF TRUE',
  `location` varchar(10000) DEFAULT NULL,
  `contact` varchar(10000) DEFAULT NULL,
  `email` varchar(10000) DEFAULT NULL,
  `website` varchar(10000) DEFAULT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`c_id`, `c_name`, `ctype`, `c_approved`, `c_verified`, `location`, `contact`, `email`, `website`, `reg_date`) VALUES
(1, 'Leapfrog', 'IT/Computer Science', 1, 0, 'Kathmandu', '987654', 'lf@gmail.com', 'www.leapfrog.com', '2017-05-16 17:14:42'),
(2, 'DJP Social Services', 'Organization(NGO/INGO)', 1, 0, 'Kathmandu', '23872628', 'djp@gmail.coim', 'djp.com.np', '2017-05-16 17:10:56'),
(3, 'Bajra Developers', 'IT/Computer Science', 1, 0, 'Kathmandu', '473687434', 'bd@gmail.com', 'www.bajra.com.np', '2017-05-16 17:11:00'),
(4, 'Encore Ltd', 'IT/Computer Science', 1, 0, 'Kathmandu', '2345567', 'encore@gmail.com', 'encore.com', '2017-05-16 17:11:06'),
(5, 'Bakery Cafe', 'Hotel/Administration', 1, 0, 'Lalitpur', '2345567', 'bc@gmail.com', NULL, '2017-05-16 17:14:45'),
(6, 'Aangan Restaurant', 'Hotel/Administration', 1, 0, 'Lalitpur', '98765678', 'aangan@gmail.com', 'aangan.com', '2017-05-16 17:14:49'),
(7, 'Skeinsoft Web', 'IT/Computer Science', 1, 0, 'Pokhara', '34567890', 'skein@fghjk.com', 'skeinsoft.com', '2017-05-16 17:11:22'),
(8, 'Bloome digital works', 'IT/Computer Science', 0, 0, 'Bhaktapur', '09876543', 'bloomedigi@fghjk.com', 'www.bloome.om', '2017-05-14 15:34:53'),
(28, 'Ronak Inc', 'Other (Shares Business)', 0, 0, 'Baneshwor', '9843692080', 'ronak@internnepal.com', 'www.ronakboss.com', '2017-05-15 18:10:45'),
(27, 'Pratik Inc', 'IT/Computer Science', 1, 1, 'Bagdol', '9843692080', 'pratik@internnepal.com', 'www.internnepal.com', '2017-05-16 17:11:31'),
(29, 'Paneru Photography', 'Other (Photo Studio)', 1, 1, 'Baneshwor', '9843692080', 'binod@internnepal.com', 'www.internnepal.com', '2017-05-16 17:11:35'),
(35, 'Registered but Shady Company', 'Other (Shady Work)', 1, 0, 'Unknown Location', '98XXXXXXXX', 'somethingillegal@shady.com', 'www.notashadysite.com', '2017-05-17 04:30:02');

-- --------------------------------------------------------

--
-- Table structure for table `internship`
--

CREATE TABLE `internship` (
  `intern_id` int(10) UNSIGNED NOT NULL,
  `c_id` int(6) DEFAULT NULL,
  `intern_title` varchar(10000) DEFAULT NULL,
  `i_approved` int(1) NOT NULL DEFAULT '1' COMMENT 'BOOLEAN. DEFAULT 1. 0 IF FALSE. ',
  `internis` varchar(1000) DEFAULT NULL,
  `opening` int(3) DEFAULT NULL,
  `deadline` varchar(1000) DEFAULT NULL,
  `internlocation` varchar(10000) DEFAULT NULL,
  `qualification` mediumtext,
  `experience` mediumtext,
  `skills` mediumtext,
  `responsibility` mediumtext,
  `apply` mediumtext,
  `internship_category` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internship`
--

INSERT INTO `internship` (`intern_id`, `c_id`, `intern_title`, `i_approved`, `internis`, `opening`, `deadline`, `internlocation`, `qualification`, `experience`, `skills`, `responsibility`, `apply`, `internship_category`) VALUES
(1, 7, 'Web Developer', 1, 'Paid', 1, '2017-06-12', 'Kathmandu', 'Bachelor running', 'NO experience required', 'Hardworking\r\nMotivational', 'Develop web based application', 'Through our email. ', 'IT/Computer Science'),
(6, 6, 'System Admin', 1, 'Paid', 1, '2017-03-19', 'Lalitpur', 'ME. CSc.', '5yrs', 'Managing Resource', 'Maintaining System', 'Through Email', 'IT/Computer Science'),
(5, 5, 'Chef', 1, 'Paid', 10, '2017-06-19', 'Lalitpur', 'Not Needed', '1yrs', 'Baking', 'Baking Cakes for Charity', 'Through Email or Phone', 'Hotel'),
(2, 1, 'Manager', 1, 'Unpaid', 5, '2017-04-19', 'Kathmandu', 'Masters in related field', '1yr', 'Managing Stock', 'Managing Assets', 'Through Email', 'Administration'),
(3, 1, 'System Admin', 1, 'Paid', 1, '2017-03-19', 'Lalitpur', 'ME. CSc.', '5yrs', 'Managing Resource', 'Maintaining System', 'Through Email', 'IT/Computer Science'),
(4, 6, 'Chef', 1, 'Paid', 10, '2017-05-19', 'Lalitpur', 'Not Needed', '1yrs', 'Baking', 'Baking Cakes for Charity', 'Through Email or Phone', 'Hotel'),
(7, 4, 'System Admin', 1, 'Paid', 1, '2017-08-19', 'Lalitpur', 'ME. CSc.', '5yrs', 'Managing Resource', 'Maintaining System', 'Through Email', 'IT/Computer Science'),
(8, 3, 'System Admin', 1, 'Paid', 1, '2017-03-19', 'Lalitpur', 'ME. CSc.', '5yrs', 'Managing Resource', 'Maintaining System', 'Through Email', 'IT/Computer Science'),
(9, 2, 'Manager', 1, 'Unpaid', 5, '2017-05-19', 'Kathmandu', 'Masters in related field', '1yr', 'Managing Stock', 'Managing Assets', 'Through Email', 'Administration'),
(10, 4, 'Web Developer', 1, 'Paid', 1, '2017-06-12', 'Kathmandu', 'Bachelor running', 'NO experience required', 'Hardworking\r\nMotivational', 'Develop web based application', 'Through our email. ', 'IT/Computer Science'),
(11, 8, 'Web Developer', 1, 'Paid', 1, '2017-06-12', 'Kathmandu', 'Bachelor running', 'NO experience required', 'Hardworking\r\nMotivational', 'Develop web based application', 'Through our email. ', 'IT/Computer Science'),
(15, 27, 'PHP Guru', 1, 'Other', 1, '2017-05-16', 'Bagdol', 'PHP chai auna paryo darroooo', 'Experience matlab vayena. Ghachyak ghuchuk milauna saknu chai paryo', 'PHP pro', 'PHP ma internnepal lai sudharney', 'Contact us by Phone or Email', 'Other'),
(16, 29, 'Photography Intern', 1, 'Other (Daily Wage)', 1, '2017-05-25', 'Can be all over Nepal', 'Not Required', 'Not Required', 'Basic Photography', 'Should Assist in professional photography', 'Send Application via Email', 'Other (Photography)'),
(22, 35, 'Helper Grunt', 1, 'Paid', 1, '2017-05-25', 'Told when You get an Assignment', 'Should be Very Strong Physically ', '5 yrs Jail Minimum', 'Pickpocketing, Espionage, Theiving, Raiding, Knife Skills', 'Protect the Boss', 'Be resourceful. Find ways to apply.', 'Other (Misc)');

-- --------------------------------------------------------

--
-- Table structure for table `request_std`
--

CREATE TABLE `request_std` (
  `req_id` int(10) NOT NULL,
  `req_fname` varchar(1000) DEFAULT NULL,
  `req_lname` varchar(1000) DEFAULT NULL,
  `req_email` varchar(1000) DEFAULT NULL,
  `req_phone` varchar(20) DEFAULT NULL,
  `req_faculty` varchar(100) DEFAULT NULL,
  `req_seeking` varchar(10000) DEFAULT NULL,
  `req_message` varchar(10000) DEFAULT NULL,
  `req_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `training_institute`
--

CREATE TABLE `training_institute` (
  `ti_id` int(10) UNSIGNED NOT NULL,
  `ti_name` varchar(10000) DEFAULT NULL,
  `ti_ctype` varchar(1000) DEFAULT NULL,
  `ti_location` varchar(10000) DEFAULT NULL,
  `ti_contact` varchar(10000) DEFAULT NULL,
  `ti_email` varchar(10000) DEFAULT NULL,
  `ti_website` varchar(10000) DEFAULT NULL,
  `ti_reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `training_institute`
--

INSERT INTO `training_institute` (`ti_id`, `ti_name`, `ti_ctype`, `ti_location`, `ti_contact`, `ti_email`, `ti_website`, `ti_reg_date`) VALUES
(1, 'Bloom Graphics House', 'IT/Computer Science', 'Lalitpur', '9843567865', 'bloom@email.com', 'www.bloomrojesh.com', '2017-05-13 06:39:08'),
(2, 'Black Hawk', 'IT/Computer Science', 'Kathmandu', '456789098765', 'bh@gmail.com', 'www.blackhawk.com.np', '2017-05-13 06:38:04'),
(4, 'City Tandoori', 'Hotel', 'Kathmandu', '456789098765', 'bh@gmail.com', 'tandoori.com.np', '2017-05-14 06:38:04'),
(3, 'Aptech', 'Other (Basic Computer Training)', 'Kathmandu', '456789098765', 'bh@gmail.com', 'Aptech.com', '2017-05-17 06:02:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `available_courses`
--
ALTER TABLE `available_courses`
  ADD PRIMARY KEY (`ac_id`),
  ADD KEY `c_id` (`ti_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `internship`
--
ALTER TABLE `internship`
  ADD PRIMARY KEY (`intern_id`),
  ADD KEY `c_id` (`c_id`);

--
-- Indexes for table `request_std`
--
ALTER TABLE `request_std`
  ADD PRIMARY KEY (`req_id`);

--
-- Indexes for table `training_institute`
--
ALTER TABLE `training_institute`
  ADD PRIMARY KEY (`ti_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `c_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `internship`
--
ALTER TABLE `internship`
  MODIFY `intern_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `request_std`
--
ALTER TABLE `request_std`
  MODIFY `req_id` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
