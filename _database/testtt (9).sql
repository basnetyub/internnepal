-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 20, 2017 at 04:02 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testtt`
--

-- --------------------------------------------------------

--
-- Table structure for table `available_courses`
--

CREATE TABLE `available_courses` (
  `ac_id` int(10) UNSIGNED NOT NULL,
  `ti_id` int(6) DEFAULT NULL,
  `ac_title` varchar(10000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `available_courses`
--

INSERT INTO `available_courses` (`ac_id`, `ti_id`, `ac_title`) VALUES
(1, 1, 'PHP OOP Training'),
(2, 2, 'CCNA, Red Hat, MCSE'),
(3, 3, 'HTML, CSS, JS, DHTML, Bootstrap'),
(4, 4, 'Nepali Cooking, Indian Cuisine');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `c_id` int(10) UNSIGNED NOT NULL,
  `c_name` varchar(10000) DEFAULT NULL,
  `ctype` varchar(1000) DEFAULT NULL,
  `c_approved` int(1) NOT NULL DEFAULT '0' COMMENT 'Boolaean. DEFAULT 0. 1 IF TRUE.',
  `c_verified` int(1) NOT NULL DEFAULT '0' COMMENT 'Boolean. DEFAULT 0. 1 IF TRUE',
  `location` varchar(10000) DEFAULT NULL,
  `contact` varchar(10000) DEFAULT NULL,
  `email` varchar(10000) DEFAULT NULL,
  `website` varchar(10000) DEFAULT NULL,
  `c_logo_name` varchar(10000) NOT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`c_id`, `c_name`, `ctype`, `c_approved`, `c_verified`, `location`, `contact`, `email`, `website`, `c_logo_name`, `reg_date`) VALUES
(58, 'Ragnarock Construction Agency', 'Other (Construction)', 1, 0, 'Kalanki', '01-5523445', '', 'ragnarock.com', '', '2017-05-20 13:46:38'),
(57, 'Paneru Photgraphy', 'Other (Photography)', 1, 1, 'Baneshwor', '01-5544553', 'paneru@photographypaneru.com', 'photographypaneru.com', '', '2017-05-20 13:47:02'),
(56, 'Shrestha Associates', 'Hotel/Administration', 1, 0, 'Thamel, Kathamandu', '01-4444555', 'associates@sharesthaassociates.com', 'www.shresthaassociates.com.np', '', '2017-05-20 13:46:27'),
(55, 'Diyo Samajik Sews', 'Organization(NGO/INGO)', 1, 0, 'Pokhara', '06-24351672', 'diyo@gmail.com', 'www.example.com', '', '2017-05-20 13:46:18'),
(54, 'Mangal Sweets', 'Hotel/Administration', 1, 0, 'Bhaktpur,Nepal', '9845326751', 'contact@mangal.hotel', 'mangalsweet.hotel', '', '2017-05-20 13:46:13'),
(53, 'Knockout System Pvt Ltd', 'IT/Computer Science', 1, 1, 'Baneshwor', '01-4421444', 'info@knockout.com.np', 'knockoutsystem.com', '', '2017-05-20 13:47:56'),
(51, 'Nimbum Consultancy ', 'Other (Daily Wages)', 1, 0, 'Lalitpur, Nepal', '01-5001176', 'info@nimbum.com', 'nimbum.com.np', '', '2017-05-20 12:50:46');

-- --------------------------------------------------------

--
-- Table structure for table `internship`
--

CREATE TABLE `internship` (
  `intern_id` int(10) UNSIGNED NOT NULL,
  `c_id` int(6) DEFAULT NULL,
  `intern_title` varchar(10000) DEFAULT NULL,
  `i_approved` int(1) NOT NULL DEFAULT '1' COMMENT 'BOOLEAN. DEFAULT 1. 0 IF FALSE. ',
  `internis` varchar(1000) DEFAULT NULL,
  `opening` int(3) DEFAULT NULL,
  `deadline` varchar(1000) DEFAULT NULL,
  `internlocation` varchar(10000) DEFAULT NULL,
  `qualification` mediumtext,
  `experience` mediumtext,
  `skills` mediumtext,
  `responsibility` mediumtext,
  `apply` mediumtext,
  `internship_category` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internship`
--

INSERT INTO `internship` (`intern_id`, `c_id`, `intern_title`, `i_approved`, `internis`, `opening`, `deadline`, `internlocation`, `qualification`, `experience`, `skills`, `responsibility`, `apply`, `internship_category`) VALUES
(46, 58, 'Manager', 1, 'Paid', 1, '2017-05-25', 'Kalanki', '', '', '', 'Manage Assets and  Balance Checkbook', 'Apply via Email', 'Administration'),
(45, 57, 'Photographer', 1, 'Depends on Skill', 2, '2017-06-11', 'Baneshwor, Kathmandu', 'None', '1yr Photography Experience', 'Photography, Image Editing', 'Assist in Macro Photography and Outdoor Photography', 'Apply via Call , Email or Website', 'Other (Photography)'),
(44, 56, 'Junior Associate', 1, 'Paid', 5, '2017-07-11', 'Thamel', 'Bachelors in CA', 'None', 'CA Skills', 'Handling and balancing Accounts, Risk Assessment', 'Apply via phone or Email. Send CV by Email or Post', 'Administration'),
(43, 55, 'Field visit for research', 1, 'Volunteer', 1, '2017-05-31', 'Will be decided later', 'Bachelor in Social Work', 'None', 'Good people skill, mass communication \r\n', 'Visit field, communicate with people, data collection', 'Send via email or through contact number.', 'Social Work'),
(42, 54, 'Cook / Waiter', 1, 'Unpaid', 3, '2017-06-08', 'Bhaktpur, Nepal', '+2 or above in any field', 'Any one can apply', 'Interest in cooking/Service.', 'Should work aid hotel staff.', 'Call us at our phone number, visit our HR department.', 'Hotel'),
(41, 53, 'Web Designer / Developer', 1, 'Depends on Skill', 2, '2017-06-02', 'Baneshwor', 'Bachelors in Computer Science or related field.', 'Minimum 1 years of Responsive Web Development Experience.', 'HTML, CSS, JS and Framework use', 'Develop Responsive websites', 'Apply via Email or our official Website.', 'IT/Computer Science'),
(40, 51, 'Communication representative, Spokesperson', 1, 'Other (Daily Wage)', 3, '2017-05-30', 'Kathmandu', 'Bachelor in related field.\r\nBBS/BBA/BSW', '', 'Fluent English Speaking skills, Good moral,Should be self-decisive, conscientious and interact well in a team environment to manage whole.', 'Conduct interactive session with client, Co-operative with HR department, learn about communication.', 'Send CV via email. You will receive a correspondence if you are selected for the first interview-list. Successful candidates will then receive a date for the second screening, upon which they shall receive the internship opportunity.', 'Other (Consultancy)');

-- --------------------------------------------------------

--
-- Table structure for table `request_std`
--

CREATE TABLE `request_std` (
  `req_id` int(10) NOT NULL,
  `req_fname` varchar(1000) DEFAULT NULL,
  `req_lname` varchar(1000) DEFAULT NULL,
  `req_email` varchar(1000) DEFAULT NULL,
  `req_phone` varchar(20) DEFAULT NULL,
  `req_faculty` varchar(100) DEFAULT NULL,
  `req_seeking` varchar(10000) DEFAULT NULL,
  `req_message` varchar(10000) DEFAULT NULL,
  `req_timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_std`
--

INSERT INTO `request_std` (`req_id`, `req_fname`, `req_lname`, `req_email`, `req_phone`, `req_faculty`, `req_seeking`, `req_message`, `req_timestamp`) VALUES
(1, 'Pratik ', 'Gautam', 'pratikmetal3@gmail.com', '9843386239', 'BSCCSIT', 'Intern in Networking', 'I have interest in computer Network, I am currently studying CCNA.', '2017-05-20 13:44:34');

-- --------------------------------------------------------

--
-- Table structure for table `training_institute`
--

CREATE TABLE `training_institute` (
  `ti_id` int(10) UNSIGNED NOT NULL,
  `ti_name` varchar(10000) DEFAULT NULL,
  `ti_ctype` varchar(1000) DEFAULT NULL,
  `ti_location` varchar(10000) DEFAULT NULL,
  `ti_contact` varchar(10000) DEFAULT NULL,
  `ti_email` varchar(10000) DEFAULT NULL,
  `ti_website` varchar(10000) DEFAULT NULL,
  `ti_reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `training_institute`
--

INSERT INTO `training_institute` (`ti_id`, `ti_name`, `ti_ctype`, `ti_location`, `ti_contact`, `ti_email`, `ti_website`, `ti_reg_date`) VALUES
(1, 'Bloom Graphics House', 'IT/Computer Science', 'Lalitpur', '9843567865', 'bloom@email.com', 'www.bloomrojesh.com', '2017-05-13 06:39:08'),
(2, 'Black Hawk', 'IT/Computer Science', 'Kathmandu', '456789098765', 'bh@gmail.com', 'www.blackhawk.com.np', '2017-05-13 06:38:04'),
(4, 'City Tandoori', 'Hotel', 'Kathmandu', '456789098765', 'bh@gmail.com', 'tandoori.com.np', '2017-05-14 06:38:04'),
(3, 'Aptech', 'Other (Basic Computer Training)', 'Kathmandu', '456789098765', 'bh@gmail.com', 'Aptech.com', '2017-05-17 06:02:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `available_courses`
--
ALTER TABLE `available_courses`
  ADD PRIMARY KEY (`ac_id`),
  ADD KEY `c_id` (`ti_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `internship`
--
ALTER TABLE `internship`
  ADD PRIMARY KEY (`intern_id`),
  ADD KEY `c_id` (`c_id`);

--
-- Indexes for table `request_std`
--
ALTER TABLE `request_std`
  ADD PRIMARY KEY (`req_id`);

--
-- Indexes for table `training_institute`
--
ALTER TABLE `training_institute`
  ADD PRIMARY KEY (`ti_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `c_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `internship`
--
ALTER TABLE `internship`
  MODIFY `intern_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `request_std`
--
ALTER TABLE `request_std`
  MODIFY `req_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
