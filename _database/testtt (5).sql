-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2017 at 08:31 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `testtt`
--

-- --------------------------------------------------------

--
-- Table structure for table `available_courses`
--

CREATE TABLE `available_courses` (
  `ac_id` int(10) UNSIGNED NOT NULL,
  `ti_id` int(6) DEFAULT NULL,
  `ac_title` varchar(10000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `available_courses`
--

INSERT INTO `available_courses` (`ac_id`, `ti_id`, `ac_title`) VALUES
(1, 1, 'PHP OOP Training'),
(2, 2, 'CCNA, Red Hat, MCSE'),
(3, 3, 'HTML, CSS, JS, DHTML, Bootstrap'),
(4, 4, 'Nepali Cooking, Indian Cuisine');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `c_id` int(10) UNSIGNED NOT NULL,
  `c_name` varchar(10000) DEFAULT NULL,
  `ctype` varchar(1000) DEFAULT NULL,
  `location` varchar(10000) DEFAULT NULL,
  `contact` varchar(10000) DEFAULT NULL,
  `email` varchar(10000) DEFAULT NULL,
  `website` varchar(10000) DEFAULT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`c_id`, `c_name`, `ctype`, `location`, `contact`, `email`, `website`, `reg_date`) VALUES
(1, 'Leapfrog', 'IT/Computer Science', 'Kathmandu', '987654', 'lf@gmail.com', 'www.leapfrog.com', '2017-05-14 15:34:27'),
(2, 'DJP Social Services', 'Organization(NGO/INGO)', 'Kathmandu', '23872628', 'djp@gmail.coim', 'djp.com.np', '2017-05-14 15:34:30'),
(3, 'Bajra Developers', 'IT/Computer Science', 'Kathmandu', '473687434', 'bd@gmail.com', 'www.bajra.com.np', '2017-05-14 15:34:34'),
(4, 'Encore Ltd', 'IT/Computer Science', 'Kathmandu', '2345567', 'encore@gmail.com', 'encore.com', '2017-05-14 15:34:37'),
(5, 'Bakery Cafe', 'Hotel/Administration', 'Lalitpur', '2345567', 'bc@gmail.com', NULL, '2017-05-14 15:34:40'),
(6, 'Aangan Restaurant', 'Hotel/Administration', 'Lalitpur', '98765678', 'aangan@gmail.com', 'aangan.com', '2017-05-14 15:34:43'),
(7, 'Skeinsoft Web', 'IT/Computer Science', 'Pokhara', '34567890', 'skein@fghjk.com', 'skeinsoft.com', '2017-05-14 15:34:48'),
(8, 'Bloome digital works', 'IT/Computer Science', 'Bhaktapur', '09876543', 'bloomedigi@fghjk.com', 'www.bloome.om', '2017-05-14 15:34:53'),
(28, 'Ronak Inc', 'Other (Shares Business)', 'Baneshwor', '9843692080', 'ronak@internnepal.com', 'www.ronakboss.com', '2017-05-15 18:10:45'),
(27, 'Pratik Inc', 'IT/Computer Science', 'Bagdol', '9843692080', 'pratik@internnepal.com', 'www.internnepal.com', '2017-05-15 17:53:49'),
(29, 'Paneru Photography', 'Other (Photo Studio)', 'Baneshwor', '9843692080', 'binod@internnepal.com', 'www.internnepal.com', '2017-05-15 18:31:08');

-- --------------------------------------------------------

--
-- Table structure for table `internship`
--

CREATE TABLE `internship` (
  `intern_id` int(10) UNSIGNED NOT NULL,
  `c_id` int(6) DEFAULT NULL,
  `intern_title` varchar(10000) DEFAULT NULL,
  `internis` varchar(1000) DEFAULT NULL,
  `opening` int(3) DEFAULT NULL,
  `deadline` varchar(1000) DEFAULT NULL,
  `internlocation` varchar(10000) DEFAULT NULL,
  `qualification` mediumtext,
  `experience` mediumtext,
  `skills` mediumtext,
  `responsibility` mediumtext,
  `apply` mediumtext,
  `internship_category` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `internship`
--

INSERT INTO `internship` (`intern_id`, `c_id`, `intern_title`, `internis`, `opening`, `deadline`, `internlocation`, `qualification`, `experience`, `skills`, `responsibility`, `apply`, `internship_category`) VALUES
(1, 7, 'Web Developer', 'Paid', 1, '2017-06-12', 'Kathmandu', 'Bachelor running', 'NO experience required', 'Hardworking\r\nMotivational', 'Develop web based application', 'Through our email. ', 'IT/Computer Science'),
(6, 6, 'System Admin', 'Paid', 1, '2017-03-19', 'Lalitpur', 'ME. CSc.', '5yrs', 'Managing Resource', 'Maintaining System', 'Through Email', 'IT/Computer Science'),
(5, 5, 'Chef', 'Paid', 10, '2017-06-19', 'Lalitpur', 'Not Needed', '1yrs', 'Baking', 'Baking Cakes for Charity', 'Through Email or Phone', 'Hotel'),
(2, 1, 'Manager', 'Unpaid', 5, '2017-04-19', 'Kathmandu', 'Masters in related field', '1yr', 'Managing Stock', 'Managing Assets', 'Through Email', 'Administration'),
(3, 1, 'System Admin', 'Paid', 1, '2017-03-19', 'Lalitpur', 'ME. CSc.', '5yrs', 'Managing Resource', 'Maintaining System', 'Through Email', 'IT/Computer Science'),
(4, 6, 'Chef', 'Paid', 10, '2017-05-19', 'Lalitpur', 'Not Needed', '1yrs', 'Baking', 'Baking Cakes for Charity', 'Through Email or Phone', 'Hotel'),
(7, 4, 'System Admin', 'Paid', 1, '2017-08-19', 'Lalitpur', 'ME. CSc.', '5yrs', 'Managing Resource', 'Maintaining System', 'Through Email', 'IT/Computer Science'),
(8, 3, 'System Admin', 'Paid', 1, '2017-03-19', 'Lalitpur', 'ME. CSc.', '5yrs', 'Managing Resource', 'Maintaining System', 'Through Email', 'IT/Computer Science'),
(9, 2, 'Manager', 'Unpaid', 5, '2017-05-19', 'Kathmandu', 'Masters in related field', '1yr', 'Managing Stock', 'Managing Assets', 'Through Email', 'Administration'),
(10, 4, 'Web Developer', 'Paid', 1, '2017-06-12', 'Kathmandu', 'Bachelor running', 'NO experience required', 'Hardworking\r\nMotivational', 'Develop web based application', 'Through our email. ', 'IT/Computer Science'),
(11, 8, 'Web Developer', 'Paid', 1, '2017-06-12', 'Kathmandu', 'Bachelor running', 'NO experience required', 'Hardworking\r\nMotivational', 'Develop web based application', 'Through our email. ', 'IT/Computer Science'),
(15, 27, 'PHP Guru', 'Other', 1, '2017-05-16', 'Bagdol', 'PHP chai auna paryo darroooo', 'Experience matlab vayena. Ghachyak ghuchuk milauna saknu chai paryo', 'PHP pro', 'PHP ma internnepal lai sudharney', 'Contact us by Phone or Email', 'Other'),
(16, 29, 'Photography Intern', 'Other (Daily Wage)', 1, '2017-05-25', 'Can be all over Nepal', 'Not Required', 'Not Required', 'Basic Photography', 'Should Assist in professional photography', 'Send Application via Email', 'Other (Photography)');

-- --------------------------------------------------------

--
-- Table structure for table `training_institute`
--

CREATE TABLE `training_institute` (
  `ti_id` int(10) UNSIGNED NOT NULL,
  `ti_name` varchar(10000) DEFAULT NULL,
  `ti_ctype` varchar(1000) DEFAULT NULL,
  `ti_location` varchar(10000) DEFAULT NULL,
  `ti_contact` varchar(10000) DEFAULT NULL,
  `ti_email` varchar(10000) DEFAULT NULL,
  `ti_website` varchar(10000) DEFAULT NULL,
  `ti_reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `training_institute`
--

INSERT INTO `training_institute` (`ti_id`, `ti_name`, `ti_ctype`, `ti_location`, `ti_contact`, `ti_email`, `ti_website`, `ti_reg_date`) VALUES
(1, 'Bloom Graphics House', 'IT/Computer Science', 'Lalitpur', '9843567865', 'bloom@email.com', 'www.bloomrojesh.com', '2017-05-13 06:39:08'),
(2, 'Black Hawk', 'IT/Computer Science', 'Kathmandu', '456789098765', 'bh@gmail.com', 'www.blackhawk.com.np', '2017-05-13 06:38:04'),
(4, 'City Tandoori', 'Hotel', 'Kathmandu', '456789098765', 'bh@gmail.com', 'tandoori.com.np', '2017-05-14 06:38:04'),
(3, 'Aptech', 'IT/Computer Science', 'Kathmandu', '456789098765', 'bh@gmail.com', 'Aptech.com', '2017-05-14 06:38:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `available_courses`
--
ALTER TABLE `available_courses`
  ADD PRIMARY KEY (`ac_id`),
  ADD KEY `c_id` (`ti_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `internship`
--
ALTER TABLE `internship`
  ADD PRIMARY KEY (`intern_id`),
  ADD KEY `c_id` (`c_id`);

--
-- Indexes for table `training_institute`
--
ALTER TABLE `training_institute`
  ADD PRIMARY KEY (`ti_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `c_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `internship`
--
ALTER TABLE `internship`
  MODIFY `intern_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
