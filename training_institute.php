<?php include 'part/config_global.php' ?>
<!DOCTYPE html>
<html>

<head>
    <title>Training Institutes -- Intern Nepal</title>
    <?php include 'part/global_meta.php' ?>
    <meta name="description" content="Intern Nepal provides a curated list of best training institutes that help advance your career, professionally, in the best possible way.">
   	<?php include 'part/cssdependencies.php' ?>
</head>
<?php include 'part/wrapper_top.php' ?>
                <?php include 'part/training.php' ?>
<?php include 'part/wrapper_bottom.php' ?>
<?php include 'part/jsdependencies.php' ?>
</html>
